#!python3
"""Part of Jurjen's klussen program
About and utility page
some program maintenance functions
"""

# intro and auxiliary routines
from general import VERSIONSTRING, dedent
from klus import Klus
import os
import logging
from urllib.parse import quote_plus

# standard routines and data
style = dedent('''\
    #stop {
      background-color: MistyRose;
      border: 2px solid DarkRed;
      border-radius: 5px;
      color: black;
      font-size: var(--small);
      text-decoration: none;
      float: right;
      position: relative;
      top: -50px;
      margin: 20px;
      z-index: 5;
      padding: 10px }
    .bigbutton {
      background-color: HoneyDew;
      border: 2px solid DarkGreen;
      border-radius: 5px;
      color: black;
      text-decoration: none;
      padding: 5px 0;
      display: inline-block;
      margin-left: 2em }
    .rabbit {
      background: url(/img/konijn.svg) no-repeat center;
      background-size: contain }
    .rabbit p, .rabbit h2 { background-color: #fffd }
    ''')

def body(klusDB):
    """Body code.
    Variable.
    """
    length = len(klusDB)
    length -= sum(k.verwijderd
                  for k in klusDB.values()
                  if isinstance(k, Klus))
    fileSize = os.path.getsize(klusDB.path) / 1024
    # PaleGreen #98FB98
    # Yellow #FF0
    # Gold #FFD700
    yield dedent(f'''\
        <div class=rabbit>
        <h2>Over dit programma</h2>
        <p>{VERSIONSTRING}</p>
        <p>De database <code>klussen.db</code> heeft
        {klusDB.entryCounter} entries
        en {length} klusjes.
        Grootte bestand: {fileSize:.1f} kiB.
        <br>
        <b>Opschonen</b> van de database
        maakt een back-up in <code>klussen.bak</code></p>
        
        <p><button type=submit name=rewrite value=0
         class=bigbutton style="background-color:#98fb98dd">
          Comprimeer database met behoud klusjes
        </button></p>
        <p><button type=submit name=rewrite value=91
         class=bigbutton style="background-color:#ff0d">
          Ruim klusjes op ouder dan 3 maanden
        </button></p>
        <p><button type=submit name=rewrite value=31
         class=bigbutton style="background-color:#ffd700dd">
          Ruim klusjes op ouder dan 1 maand
        </button></p>
        
        ''')
    try:
        import util
        changes = False
        import difflib
        for dev, prod in util.genFiles(True):
            outname = f"inc/{dev[:-3]}diff.html"
            # if os.path.exists(outname): continue
            if not changes:
                changes = True
                yield dedent('''\
                    <p>Changes:</p>
                    <table>
                    ''')
            with open(prod) as prodFile:
                prodLines = prodFile.readlines()
            with open(dev) as devFile:
                devLines = devFile.readlines()
            with open(outname, 'w') as outfile:
                outfile.write(
                    difflib.HtmlDiff(4).make_file(
                    prodLines,
                    devLines,
                    prod,
                    dev,
                    5
                )   )
            old = set(prodLines)
            new = set(devLines)
            yield dedent(f'''\
                <tr><td><a href="{outname}">{dev}</a>
                </td><td>+{len(new-old)}-{len(old-new)}
                </td></tr>
                ''')
        if changes:
            yield '</table>'
    except ImportError:
        pass
    yield dedent('''\
        </div>
        <a href="/__exit" id=stop>
        Stop server</a>
        ''')

script = ''

def post(forms, klusDB, klusID):
    """Process post data
    """
    if forms.rewrite:
        days = int(forms.rewrite)
        if days:
            klusDB.rewrite(int(days))
        else:
            klusDB.rewrite()
        message = quote_plus("Database bijgewerkt.")
        return 'about?m=' + message

def clearDiffs():
    clearFlag = False
    for name in os.listdir('inc'):
        if name.endswith('diff.html'):
            os.unlink('inc/' + name)
            clearFlag = True
    if clearFlag: logging.info("Deleted diff files")
