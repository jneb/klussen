This is my personal life planning tool.
Since there is no serious followup of the great program Life Balance, this is the only way
for me to get a useful planning tool.
This program is written specifically for use under qPython on an Android phone,
but is written to work independently of that.
Let me know what you think.

One pretty cool feature of this program is that is improves on Bottles interface
(the version that is in qpython at the moment)
by allowing caching and compression.
The caching actually speeds up the program a lot!

I am planning to write a version with an English interface to improve the usefulness of this program.
