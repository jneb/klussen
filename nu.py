#!python3
"""Jurjen's planning program
main page: current task list
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
from datetime import datetime
from klusdb import KlusInfo
from general import dedent

def listItem(relevance, ID, klus, code):
    relclass = 2 if relevance >= 1 else 1 if relevance >= .5 else 0
    return dedent(f'''\
        <tr>
          <td class=key>{code}</td>
          <td><input type=checkbox name="{ID}" title="Klaar"
               onchange="emphasizeSend()">
          </td>
          <td><a href="/edit/{ID}"
               class="rel{relclass}">{klus.naam or "(Leeg)"}</a></td>
          <td><img src="/img/{klus.icon}.png" height=28></td>
          <td class=numcol>{relevance:03.0%}</td>
          <td class=ifwide><meter value="{klus.energie / 255:.2f}"></meter></td>
        </tr>
        ''')

# standard routines and data
style = dedent('''\
    #tasktable { width: 100% }
    #tasktable tr { background-color: white }
    #tasktable tr:nth-child(even) { background-color: LightYellow }
    #tasktable th { text-align: left }
    input[type=checkbox] {
      transform: scale(1.5);
      margin-right: 18px }
    #tasktable td a {
      text-decoration: none;
      display: block;
      color: black }
    #tasktable td a.rel2 { font-weight: bold }
    #tasktable td a.rel1 {}
    #tasktable td a.rel0 { font-style: italic }
    .numcol { text-align: right }
    #tasktable td img { margin: -4px 0 }
    #tasktable meter { width:4em }
    ''')


def body(klusDB):
    yield dedent('''\
        <table id=tasktable><thead>
        <tr>
         <th>OK</th>
         <th>Beschrijving</th>
         <th></th>
         <th class=numcol>%</th>
         <th class=ifwide>Energie</th>
        </tr></thead><tbody>
        ''')
    klusinfo = KlusInfo(klusDB)
    klusList = [
        (klusinfo.relevantie(klus), ID, klus)
        for ID,klus in klusDB.items()
        if not klus.verwijderd
        ]
    klusList.sort(reverse=True)
    for rel,ID,klus in klusList:
        if rel <= 0: break
        if klusinfo.waiting(klus): continue
        yield listItem(rel, ID, klus, klusinfo.filterCode(klus))
    yield '</tbody></table>\n'


script = ""

def post(forms, klusDB, klusID):
    """Process post data
    only handles tags defined here
    """
    for name,value in forms.items():
        if value == 'on' and name in klusDB:
            klusDB[name].addGedaan(datetime.now())
            klusDB.journal(name)
    return "/nu"
