#!python3
"""Jurjen's planning program
bottom functions: new klus, send
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
from klus import Klus
import logging
from general import dedent

# standard routines and data
style = dedent('''\
    footer {
      position: sticky;
      width: 100%;
      background-color:#fff8;
      bottom: 0;
      z-index: 2 }
    footer input {
      margin: 4px 0 4px 2px;
      background-color: HoneyDew;
      border-radius: 10px }
    input[type=submit] {
      color: DarkGreen;
      font-size: var(--entryfs);
      border-radius: 10px }
    #send {
      transition: font-weight 1s, border-width 1s }
    #new {
      color: grey;
      width: 7em;
      border-radius: 4px;
      margin-left: 4px;
      transition: width 1s ease-out }
    #new:focus {
      color: black;
      width: 14em }
    #taskcount {
      color: Grey;
      padding: 2px;
      background-color: white }
    ''')


def body(pagename):
    """Body code."""
    yield dedent('''\
        <footer>
          <input type=text id=new name=new
           placeholder="Nieuwe klus..."
           onkeyup="emphasizeSend();enterSubmit(event)">
          <input type=submit id=send
           value=Stuur>
          <span id=taskcount></span>
        </footer>
        ''')
    if pagename in ('Alles', 'Nu'):
        yield f'<script>doFilter("{pagename}")</script>'

script = dedent('''\
    function emphasizeSend() {
      button = document.getElementById('send');
      button.style.color = 'FireBrick';
      button.style.fontWeight = "bold";
      button.style.border = "3px solid FireBrick";
    }
    function enterSubmit(event) {
      if (event.keyCode == 13) {
        document.getElementById('theform').submit();
      }
    }
    ''')


def post(forms, klusDB):
    """Process post data
    only handles tags defined here
    ID: on   means to set gedaan to now
    new: text means nieuwe klus with text
    """
    if forms.suppressed and klusDB.suppressed != forms.suppressed:
        klusDB.suppressed = forms.suppressed
        klusDB.flush()
        logging.info("Saving suppressed: %s", forms.suppressed)
    if forms.minrelevance and klusDB.minrelevance != int(forms.minrelevance):
        klusDB.minrelevance = forms.minrelevance
        logging.info("Saving minrelevance: %s", forms.minrelevance)
        klusDB.flush()
    if forms.new:
        klusID = klusDB.newID()
        klus = Klus(forms.new)
        klusDB[klusID] = klus
        return klusID
