#!python3
"""Database manager for planning
Klus record
"""


from datetime import datetime, timedelta
from random import randrange
from collections import namedtuple, ChainMap
from general import PLANPERIOD, DBWIDTH, lineGrouper
import textwrap
import logging
from itertools import count

class Record:
    """A record in this database
    Most are tasks, but there are exceptions.
    Each had these methods:
    """
    @classmethod
    def fromYAML(cls, value, remainder):
        """Construct from YAML short form (only value)
        our long form (with remainder of lines
        """
        raise NotImplementedError

    def replacesEntry(self, other):
        """indicate that self will be replaced
        by other. Adjust self if needed
        only used for entries that registrer finished date
        """


class Attr:
    """A task attribute: base class
    every attribute has a default value
    """
    def __init__(self, name, default):
        self.name = name
        self.default = default

    def __repr__(self):
        return f'Attr({self.name})'

    def toYAMLentry(self, name, value, indent=2):
        """Make yaml entry if not default
        excluding indent and last newline
        """
        if value == self.default:
            return ""
        return name + ': ' + self.toYaml(value, indent + 2)

    def toHTML(self, value):
        """convert to HTML excluding quotes
        """
        raise NotImplementedError

    def fromHTML(self, value):
        return self.checkSet(value)

    def checkSet(self, value):
        logging.debug("Setting %s to %s", self.name, value)
        return value

    def fromYAML(self, value, group):
        return self.checkSet(value)


class StrAttr(Attr):
    """String
    Strings with newlines are laid out with an initial |
    followed by the lines (indented)
    Long strings are formatted with an initial >
    followed by indented lines that will be joined
    """
    def __init__(self, name, default=""):
        super().__init__(name, default)

    def toYaml(self, value, indent=4):
        """Convert to YAML format
        can be multiline
        assumes attr: in front
        """
        if not value:
            return '""'
        elif '\n' in value:
            return "|\n" + " " * indent + ("\n" + " " * indent).join(value.splitlines())
        elif len(value) > DBWIDTH - len(self.name) - 4:
            return ">\n" + textwrap.fill(
                value,
                width=DBWIDTH,
                initial_indent=" "*indent,
                subsequent_indent=" "*indent,
                break_on_hyphens=False)
        elif value[0] in "|>":
            return "|\n" + " " * indent + value
        else:
            return value

    def fromYAML(self, firstLine, remainder):
        if firstLine == '""':
            result = ""
        elif firstLine == '|':
            # the lines in remainder end with newline
            result = '\n'.join(
                line[4:].rstrip()
                for line in remainder)
        elif firstLine == '>':
            result = ' '.join(
                line.strip()
                for line in remainder)
        else: result = firstLine
        return self.checkSet(result)

    def toHTML(self, value):
        """convert to HTML excluding quotes
        """
        return value

    def fromHTML(self, value):
        """convert from HTML without quotes
        """
        return self.checkSet(value)

    def checkSet(self, value):
        if len(value) > 2048 or len(set(value)) > 90:
            raise ValueError("Unlikely value", value, "for", self.name)
        return value


class IntAttr(Attr):
    def toYaml(self, value, indent=4):
        """Convert to YAML format
        assumes attr: in front
        """
        return str(value)

    def fromYAML(self, value, remainder):
        """Convert from YAML format
        """
        return self.checkSet(int(value))

    def toHTML(self, value):
        """convert to HTML excluding quotes
        """
        return str(value or self.default)

    def fromHTML(self, value):
        """convert from HTML without quotes
        """
        return self.checkSet(int(value))

    def checkSet(self, value):
        if abs(value) >= 10**10:
            raise ValueError("Unlikely value", value, "for", self.name)
        return value


class DtAttr(Attr):
    """datetime or None"""
    def __init__(self, name, default=None):
        super().__init__(name, default)

    def toYaml(self, value, indent=4):
        """Convert to YAML format
        can be multiline
        assumes attr: in front
        """
        return value.strftime("%Y-%m-%d@%H:%M")

    def fromYAML(self, value, remainder):
        """Convert from YAML format
        including multiline
        (for the caller to figure out!)
        """
        return self.checkSet(datetime.strptime(value, "%Y-%m-%d@%H:%M"))

    def toHTML(self, value):
        """convert to HTML excluding quotes
        """
        if value is None:
            return ""
        else:
            return value.strftime("%Y-%m-%dT%H:%M")

    def fromHTML(self, value):
        """convert from HTML without quotes
        """
        if not value:
            return None
        else:
            return self.checkSet(datetime.strptime(value, "%Y-%m-%dT%H:%M"))


class BoolAttr(Attr):
    def toYaml(self, value, indent=4):
        """Convert to YAML format
        can be multiline
        assumes attr: in front
        """
        return ("No", "Yes")[value]

    def fromYAML(self, value, remainder):
        """Convert from YAML format
        including multiline
        (for the caller to figure out!)
        """
        return {"No": False, "Yes": True}[value]

    def toHTML(self, value):
        """convert to HTML excluding quotes
        Boolean produces "checked"
        """
        return "checked" * value

    def fromHTML(self, value):
        """convert from HTML without quotes
        """
        return value == "on"


class ChecklistAttr(Attr):
    """Internally, a dict from string to bool
    """
    def toYaml(self, value, indent=4):
        """Convert to YAML format
        normally multiline with * at first line item
        with * or _ in front of the items
        short form is [* item; _ item]
        assumes attr: in front
        """
        if (sum(map(len, value))
            + 4 * len(value)
            + 8 < 2 * DBWIDTH
            and {';', '\n'}.isdisjoint(set(''.join(value)))):
            return '[' + '; '.join(
                '_*'[flag] + ' ' + item
                for item,flag in value.items()
                ) + ']'
        result = "*"
        for item,flag in value.items():
            # remove trouble chars
            if not item.isprintable():
                item = ''.join(filter(methodcaller('isprintable'), item))
            result += f'\n{"":{indent}s}{"_*"[flag]} ' + item
        return result

    def fromYAML(self, firstline, remainder):
        """Convert from YAML format
        including multiline
        (for the caller to figure out!)
        """
        result = {}
        if firstline.startswith("[") and firstline.endswith("]"):
            remainder = firstline[1:-1].split(';')
        elif firstline != '*':
            raise SyntaxError("Checklist")
        for line in remainder:
            flagged = line.strip()
            if not flagged: continue
            if flagged[0] not in '_*': raise SyntaxError
            result[flagged[1:].strip()] = flagged[0] == '*'
        return result

    def toHTML(self, value):
        """convert to HTML excluding quotes
        makes checkboxes with name=naam[i]
        """
        return "\n".join(
            "<li><label>"
            "<input type=checkbox title=Klaar "
            f"name={self.name}[{i}]{' checked' * value}>"
            + item +
            "</input></label></li>"
            for i,(item,value) in enumerate(value.items())
            )

    def fromHTML(self, current, postdata):
        """convert from HTML
        in contrast to the other attributes,
        we get current value and postdata
        we get all post data to check
        """
        if not current: return
        result = dict.fromkeys(current, False)
        for i,name in enumerate(current):
            key = f"{self.name}[{i}]"
            if key in postdata:
                result[name] = True
        return result

    def fromSerial(self, value):
        return {line[1:]: line[0] == '*'
                for line in value.splitlines()
                if len(line) > 1}


class Klus(Record):
    """Container class for task
    You can set values to None to delete
    """
    
    fields = {
        field.name: field
        for field in (
            StrAttr("naam"),
            StrAttr("doel", "Persoonlijk"),
            StrAttr("context", "Algemeen"),
            StrAttr("icon", "circled-thin"),
            IntAttr("energie", 63),
            DtAttr("gedaan"),
            StrAttr("eerst"),
            BoolAttr("herhaal", False),
            IntAttr("min", 5),
            IntAttr("max", 10),
            DtAttr("gepland"),
            DtAttr("vanaf"),
            DtAttr("tot"),
            StrAttr("notitie"),
            ChecklistAttr("stappen", None),
            BoolAttr("verwijderd", False)
            )
        }
    defaults = {attr:descr.default
                for attr,descr in fields.items()}

    # default value
    eerderGedaan = ()

    # during name change of attributes
    # allow old names in db
    altname = {"naam": "name"}
    
    def __init__(self, naam=""):
        self.__dict__.update(self.defaults)
        self.naam = naam

    def __bool__(self):
        return bool(self.naam)

    def __repr__(self):
        return f'Klus({self.naam})'

    def dump(self, indent=2):
        """Write YAML of record without header
        omitting default and empty values
        """
        if self.herhaal:
            self.vanaf = self.tot = None
        else:
            self.min = self.max = None
        result = []
        for attr,descr in self.fields.items():
            if self.verwijderd and attr not in ('verwijderd', 'naam'):
                continue
            value = getattr(self, attr)
            if value is None:
                continue
            entry = descr.toYAMLentry(attr, value)
            if entry:
                result.append(entry)
        if (sum(map(len, result))
            + 2 * len(result)
            + 4 < 2 * DBWIDTH
            and {",", "\n"}.isdisjoint(set(''.join(result)))
           ):
            # short form, at most 2 lines
            return ": {" + ", ".join(result) + "}"
        return ':\n' + " " * indent + ("\n" + " " * indent).join(result)

    def fromPost(self, postData):
        """Update from post data
        ignores entries that aren't field
        """
        for name, field in self.fields.items():
            if isinstance(field, ChecklistAttr):
                # checklist needs separate treatment
                setattr(self, name, field.fromHTML(getattr(self, name), postData))
            else:
                setattr(self, name, field.fromHTML(postData.get(name, "")))

    @property
    def grace(self):
        """Time between 0% and 100%
        for relevance computation
        """
        if self.herhaal:
            return timedelta(days=self.max - self.min)
        if self.vanaf and self.tot:
            return self.tot - self.vanaf
        return timedelta(hours=12)

    def isPlanned(self, now=None):
        """Check if task is waiting for planned moment
        That is, from now
        until half PLANPERIOD
        after the planned moment
        If it ready, it still counts as planned!
        """
        if not self.gepland:
            return False
        if now is None:
            now = datetime.now()
        return now < self.gepland - PLANPERIOD / 2
        
    def finished(self):
        """Determine if task is finished
        """
        now = datetime.now()
        if not self.herhaal:
            return bool(self.gedaan)
        elif not self.gedaan:
            return False
        elif now < self.gedaan + timedelta(days=self.min):
            return True
        else:
            return self.isPlanned(now)

    def relevantie(self):
        """This routine is the essence of the program
        calculate relevance to determine the order
        """
        now = datetime.now()
        try:
            if self.isPlanned(now):
                return (now - self.gepland) / PLANPERIOD + .5
            elif self.herhaal:
                # 0 to 1 after min to max days
                return (now - self.gedaan - timedelta(days=self.min)) / self.grace
            elif self.gedaan:
                # gradually become lower from 0 down
                return (self.gedaan - now) / self.grace
            elif self.vanaf:
                # 0 tot 1 from vanaf to tot
                return (now - self.vanaf) / self.grace
            else: return 1.01
        except Exception:
            return 1.02

    # for html forms
    def html(self, attr, offset=None):
        """Make value for HTML use
        in case of datetime, use offset to get a default
        of now + given number of minutes
        """
        descr = self.fields[attr]
        value = getattr(self, attr)
        if offset is not None and value is None and isinstance(descr, DtAttr):
            value = datetime.now() + timedelta(minutes=offset)
        return self.fields[attr].toHTML(value)

    # for parser
    @classmethod
    def fromYAML(cls, firstLine, remainder):
        if firstLine:
            # short form, all items single line
            if not (firstLine.startswith('{') and firstLine.endswith('}')):
                raise SyntaxError
            result = cls()
            for item in firstLine[1:-1].split(','):
                attr, sep, value = item.partition(':')
                if not sep: raise SyntaxError
                attr = attr.lstrip()
                if attr not in result.fields:
                    attr = Klus.altName["attr"]
                setattr(result, attr, result.fields[attr].fromYAML(value.strip(), ()))
            return result
        #  long form, split into groups for longer items
        result = Klus()
        for group in lineGrouper(remainder, "  "):
            result.parseItems(group)
        return result

    def copy(self):
        """Make copy, but not dpne"""
        result = Klus(self.naam + " (kopie)")
        for attr, field in self.fields.items():
            if attr not in ('naam', 'gedaan'):
                setattr(result, attr, getattr(self, attr))
        return result

    def parseItems(self, group):
        """Parse the lines of a record item
        """
        line = next(group)
        attr, sep, value = line.partition(':')
        if not sep:
            raise SyntaxError(f'colon expected in "{line}"')
        attr = attr.strip()
        try:
            setattr(self, attr, self.selectParser(attr)(value.strip(), group))
        except SyntaxError:
            raise SyntaxError(f'error in record')
        for line in group:
            raise SyntaxError(f'unexpected indented line "{line}"')
            
    def selectParser(self, attr):
        return self.fields[attr].fromYAML

    def alleGedaan(self):
        if not self.gedaan:
            return
        yield from self.eerderGedaan 
        yield self.gedaan

    def addGedaan(self, dt):
        """Mark as gedaan:
        if dt is None, remove last
        otherwise, add to
        eerderGedaan + gedaan
        make attribute if needed
        """
        if dt == self.gedaan:
            return
        if dt is None:
            if self.eerderGedaan:
                (*self.eerderGedaan, self.gedaan) = self.eerderGedaan
                if not self.eerderGedaan:
                    del self.eerderGedaan
        else:
            # add new dt
            if self.gedaan:
                self.eerderGedaan = (*self.eerderGedaan, self.gedaan)
            self.gedaan = dt
            # remove planned date
            self.gepland = None

    def replacesEntry(self, other):
        """Update other with self.gedaan
        and copy result back to self
        """
        # add self gedaan to other and copy
        other.addGedaan(self.gedaan)
        self.gedaan = other.gedaan
        if other.eerderGedaan:
            self.eerderGedaan = other.eerderGedaan

if __name__ == "__main__":
    Klus.lineno = 'test'
    import pdb
    pdb.set_trace()
    try:
        it = iter('''
  naam: test
  stappen: *
    * Stap 1
    _ Stap 2'''.splitlines())
        print(Klus.fromYAML(next(it), it).dump())
        Klus.lineno = 'test'
        it = iter('''
  naam: test
  stappen: [* Stap 1;_ Stap 2;* Stap 3; _Stap 4;_Stap 5;_Stap 6;_Stap 7;_Stap 8;*Stap 9]'''.splitlines())
        print(Klus.fromYAML(next(it), it).dump())
    except Exception as e:
        print(e)
        pdb.post_mortem()
