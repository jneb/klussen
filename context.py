#!python3
"""Jurjen's planning program
context editor
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
import klusdb
from general import makeOption, dedent, openQuote, closeQuote
from collections import Counter
from operator import attrgetter
from html import escape
import logging
from urllib.parse import quote_plus

# standard routines and data
style = dedent('''\
    #mergetable {
      font-size: var(--entryfs);
      border-spacing: 10px 0;
      cursor: context-menu }
    #mergetable a {
      text-decoration: none;
      color: black;
      display: block;
      padding: 0 }
    #mergetable td img { vertical-align: middle }
    #schedulediv { border-spacing: 10px 5px }
    #schedulediv .center button, .center input {
      display: block;
      margin: 4px auto;
      font-size: 18px;
      width: 2em;
      padding: 5px }
    #schedulediv .center button {
      background-color: DarkGreen;
      color: white;
      text-align: center }
    #mergetable + div {
      z-index: 1
    }
    #edittable {
      background-color: #eafbfb;
      padding-bottom: 3px;
      transition: margin .4s ease-out }
 }
    ''')

def body(klusDB):
    """Body code.
    Variable.
    """
    yield dedent('''\
        <div id=edittable>
        <h2>Contexten:</h2>
        <div class="menu right">
        <table id=mergetable><thead>
        <tr><th>Naam
        </th><th>#
        </th><th>Wanneer
        </th></tr></thead>
        ''')
    counts = Counter(map(attrgetter("context"), klusDB.values()))
    for c,context in enumerate(klusDB.contexten(), start=97):
        if context in klusDB.ctxTimes:
            days, hours = klusDB.ctxTimes[context]
            days = sum(1<<d for d in days)
            start, end = min(hours), max(hours) + 1
            timeStr = klusDB.ctxTimes.dumpSubset(context)[len(context)+2:]
        else:
            days, start, end = 127, 0, 24
            timeStr = 'Altijd'
        yield dedent(f'''\
            <tr onclick="setupMenu('{context}',{days},{start},{end})">
            <td>{escape(context)}
            </td><td class=numcol>
              <a href="/alles/context?q={chr(c)}">
              {counts[context]}\N{north east arrow}
              </a>
            </td><td>{timeStr}
            </td></tr>
            ''')
    yield dedent('''
        </table>
        <label><input type=text name=newcontext
          placeholder="Nieuwe context" onkeyup="emphasizeSend()"
        ></label>
        <div id=editmenu>
        ''')
    yield from makeSettingsMenu(klusDB)
    yield '</div></div></div>'


renameOrMergeStr = dedent('''\
    <label><input type=text name=naam id=naam title=Rename
     onkeyup="emphasizeSend()"></label>
    <input type=hidden name=orig id=orig>
    <label><input type=checkbox
     onchange="if(!this.checked)this.parentElement.nextElementSibling.value=''">
    Voeg samen met</input></label>
    <select name=merge value="" size=8
     onchange="this.previousElementSibling.firstElementChild.checked=true;emphasizeSend()"
     aria-label=Choose
     >
    ''')

def makeSettingsMenu(klusDB):
    yield renameOrMergeStr
    yield from makeOption(klusDB.contexten())
    yield dedent('''\
        <table id=schedulediv>
        <tr><th>Weekdagen</th>
          <th colspan=3>Tijd</th></tr>
        <tr><td>
            <label><input type=checkbox name=maandag>
              Maandag</input></label></td>
          <td colspan=3>Periode:</td></tr>
        <tr><td><label><input type=checkbox name=dinsdag>
            Dinsdag</input></label></td>
          <td rowspan=5 class=center>
            <button type=button onclick='adjust("start", 1)'>
              +</button>
            <input type=number min=0 max=23 name=starttime id=start value=0
             aria-label=Van>
            <button type=button onclick='adjust("start", -1)'>
              &ndash;</button>
            </td><td></td>
          <td rowspan=5 class=center>
            <button type=button onclick='adjust("end", 1)'>
              +</button>
            <input type=number min=1 max=24 name=endtime id=end value=24
             aria-label=Tot>
            <button type=button onclick='adjust("end", -1)'>
              &ndash;</button></td></tr>
        <tr><td>
            <label><input type=checkbox name=woensdag>
              Woensdag</input></label></td></tr>
        <tr><td>
            <label><input type=checkbox name=donderdag>
              Donderdag</input></label></td>
          <td>tot</td></tr>
        <tr><td>
            <label><input type=checkbox name=vrijdag>
              Vrijdag</input></label></td>
        </tr>
        <tr><td>
            <label><input type=checkbox name=zaterdag>
              Zaterdag</input></label></td></tr>
        <tr><td>
            <label><input type=checkbox name=zondag>
              Zondag</input></label></td>
        </tr>
        </table>
        ''')


script = dedent('''\
    // adjust setup menu
    function setupMenu(context, days, start, end) {
      let editmenu = document.getElementById("editmenu");
      if (editmenu.classList.contains("show")) {
        document.getElementById("naam").value = context;
        document.getElementById("orig").value = context;
        document.getElementById('edittable').style.marginBottom = editmenu.scrollHeight + 'px';
        emphasizeSend();
        let schedulediv = document.getElementById("schedulediv");
        if (typeof schedulediv === "undefined") return;
        let inputList = schedulediv.getElementsByTagName("INPUT");
        inputList[2].value = start;
        inputList[3].value = end;
        var i;
        for (i=0; i<7; i++) {
          inputList[i<2?i:i+2].checked = days >> i & 1;
        }
      } else {
      	  // remove form data
        document.getElementById("orig").value = "";
    } }
    // increment or decrement counter
    function adjust(id, delta) {
      let item = document.getElementById(id);
      let val = Number(item.value);
      val += delta;
      if (val < 0) return;
      if (val > 24) return;
      item.value = val;
      if (Number(document.getElementById("start").value)
         < Number(document.getElementById("end").value)
      ) return;
      	item.value = val - delta;
    }
    ''')


def post(forms, klusDB, klusID, attr="context"):
    """Process post data for context and doel
    only handles tags defined here
    klusID is unused here
    naam orig merge
    starttijd eindtijd
    maandag dinsdag woensdag donderdag vrijdag zaterdag zondag
    """
    oldname = forms.orig
    # check for merge, rename
    newname = forms.merge or forms.naam
    if oldname and newname and newname != oldname:
        logging.info(
            "Renaming %s %s into %s",
            attr,
            oldname,
            newname)
        for klusID, klus in klusDB.items():
            if getattr(klus, attr) == oldname:
                setattr(klus, attr, newname)
                klusDB.journal(klusID)
        klusDB.suppressed = ""
        klusDB.writeHeader()
        klusDB.flush()
        return "/" + attr + "?m=" + quote_plus(
            f"Context {openQuote}{oldname}{closeQuote} hernoemd "
            f"tot {openQuote}{newname}{closeQuote}")
    # compute contextvalid string (context only)
    if oldname and attr == 'context':
        days = [d
            for d,day in enumerate(
                "maan,dins,woens,donder,vrij,zater,zon".split(','))
            if day + "dag" in forms]
        hours = range(int(forms.starttime), int(forms.endtime))
        klusDB.ctxTimes[oldname] = days, hours
        klusDB.writeHeader()
        klusDB.flush()
    # check for new context
    newattr = forms["new" + attr]
    if newattr:
        if len(getattr(klusDB, attr + "en")()) > 25:
            # make sure encoding a-z can work
            return f'{attr}?m=' + quote_plus(f"Te veel {attr}en!")
        # make task with this context, save
        klus = klusdb.Klus(attr.capitalize() + ": " + newattr)
        klusID = klusDB.newID()
        setattr(klus, attr, newattr)
        klusDB[klusID] = klus
        klusDB.suppressed = ""
        klusDB.journal(klusID)
        klusDB.writeHeader()
        klusDB.flush()
        logging.info("created: %s", klusID)
        return "edit/" + klusID
    return attr
