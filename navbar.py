#!python3
"""Jurjen's planning program
navigation bar
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
from enum import IntEnum
from html import escape
from general import dedent, openQuote, closeQuote

class MenuType(IntEnum):
    Nu = 0
    Alles = 1
    Klaar = 2
    Edit = 3
    Steps = 4
    Doel = 5
    Context = 6
    Icons = 7
    Over = About = 8

def deleteMenuEntry(klusDB, currentPage):
    """Create HTML for the delete menu entry
    """
    props = 'type=submit name=menu class=menubutton form="theform" formnovalidate'
    text = "Verwijder klus"
    if klusDB.deleted:
        props += ' formaction="/undo"'
        text = f'Herstel klus {openQuote}{klusDB[klusDB.deleted].naam}{closeQuote}'
    elif currentPage == 'Edit':
        props += ' value=delete'
    else:
        props += ' disabled'
    return '<button ' + props + '>' + text + '</button>'

def copyMenuEntry(klusDB, currentPage):
    """Create HTML for the copy menu entry
    """
    props = 'type=submit name=menu value=copy class=menubutton form="theform" formnovalidate'
    text = "Kopieer klus"
    if currentPage != 'Edit':
        props += ' disabled'
    return '<button ' + props + '>' + text + '</button>'

# nav: the navigation bar with links and menus
# menu icons are forced of the right height
# .menu: class for menu
#  first element opens the menu
#  div element is the contents hidden
#  buttons are shown like nav links

# standard routines anddata
style = dedent('''\
    nav {
      position: fixed;
      top: 0; left: 0;
      height: var(--menuheight);
      font-size: var(--menufs);
      background-color: HoneyDew;
      z-index: 5;
      box-shadow: 2px 4px 8px rgba(0,0,0,0.3) }
    nav > * { float: left }
    nav a {
      display: block;
      padding: 9px;
      text-decoration: none;
      color: DarkGreen;
      height: 100% }
    nav > div > img { height: var(--menuheight) }
    nav .active { background-color: PaleGreen }
    
    .menu { position: relative } /* position anchor */
    .menu img.disabled { opacity: 0.4 }
    .menu > div {
      display: none;
      position: fixed;
      left: 5%;        /* left if wide */
      width: 90%;       /* 25 em if wide */
      background-color: HoneyDew;
      box-shadow: 5px 8px 16px rgba(0,0,0,0.6);
      z-index: 5;
      margin-bottom: 16px;
      padding: 0 2px;
      max-height: 0;
      overflow: hidden;
      transition: max-height .3s ease-out }
    .menu.narrow > div { width: 9em }
    .menu > div button {
      background-color: inherit;
      border: none;
      font-size: inherit;
      color: DarkGreen;
      padding: 10px;
      text-align: left;
      display: block;
      margin: 0 }
    .menu button:disabled,
    nav a.disabled { color: LightGrey }
    .topline { border-top: 1px solid black }
    .bottomline { border-bottom: 1px solid black }

    .show { display: block !important }

    .menu > div a {
      float: none;
      color: DarkGreen;
      padding: 12px 10px;
      text-decoration: none;
      display: block }
    .menu > div input,
    #contextbuttons button,
    #doelbuttons button {
      margin: 4px 2px;
      text-align: center }
    .menu > div input[type=text] {
      padding: 10px 10px }
    #pattern {
      background-image: url("/img/search.png");
      background-repeat: no-repeat;
      background-size: 40px;
      min-width: 10em;
      font-size: var(--normal);
      padding: 8px 12px 8px 42px;
      text-decoration: none;
      display: block;
      text-align: left;
      width: 10em;
      border: 1px solid Grey;
      border-radius: 5px }
    #pattern + div {
      padding: 5px 0; /* for relevance slider */ }
    #minrelevance { width: 15em }
    .rotatedlabel {
      writing-mode: vertical-rl;
      transform: rotate(180deg);
      float: left;
      font-size: var(--normal) }
    #contextbuttons button, #doelbuttons button {
      font-size: var(--small);
      padding: 5px 2px;
      border: 2px solid Grey;
      border-radius: 5px;
      min-width: 4em;
      color: Grey;
      display: inline-block }
    #contextbuttons button.on {
      background-color: PaleTurquoise;
      color: black }
        #doelbuttons button.on {
      background-color: MistyRose;
      color: black }
    ''')


def body(klusDB, currentPage, debug=True):
    """Body code.
    Variable.
    """
    yield '<nav>\n'
    active = MenuType[currentPage]
    if currentPage == 'Steps': active = MenuType.Edit
    # file depends on debug mode
    icon = "rabbit on green" if debug else "konijn"
    yield dedent(f'''\
        <div class="menu narrow">
          <img alt="Rabbit menu" src="/img/{icon}.svg" height=45>
          <div id=commandmenu>
          <a href="/context">Contexten</a>
          <a href="/doel">Doelen</a>
          <a href="/icons" class=bottomline>Icons</a>
          {deleteMenuEntry(klusDB, currentPage)}
          {copyMenuEntry(klusDB, currentPage)}
          <a href="/about" class=topline>Info / Beheer DB</a>
          </div>
        </div>''')
    for page in map(MenuType, range(4)):
        link = '#' if page == MenuType.Edit else '/' + page.name.lower()
        title = page.name
        if page == active:
            cls = 'active'
        elif page != MenuType.Edit:
            cls = ''
        elif active > MenuType.Edit:
            title = active.name
            cls = 'active'
        else:
            cls = 'disabled'
        if cls: cls = f' class="{cls}"'
        yield f'<a href="{link}"{cls}>{title}</a>'
    yield dedent('''\
        <div class=menu>
          <img alt=Search src="/img/search.png" height=45
        ''')
    # at this point, the img tag is still open
    if active not in (MenuType.Nu, MenuType.Alles):
        # no search menu, but make div for menu
        yield ' class=disabled><div>'
    else:
        yield dedent('''\
            >
            <div onclick="emphasizeSend()" id=filtermenu>
            ''')
        # show search/filter menu
        # set filter buttons
        contextStr = doelStr = ""
        if klusDB.suppressed:
            contextStr, doelStr = klusDB.suppressed.split(";")
        disableSlider = " disabled" * (active == MenuType.Alles)
        yield dedent(f'''\
            <input type=text id=pattern placeholder="Zoek ..."
              onkeyup="doFilter('{currentPage}')"
              onfocus="this.value='';doFilter('{currentPage}')">
            <div><label>Relevantie:
            <input type=range id=minrelevance name=minrelevance
             value="{klusDB.minrelevance}" min=0 max=100
             onchange="doFilter('{currentPage}');emphasizeSend()"{disableSlider} form="theform">
            </label></div>
            <div id=contextbuttons>
            <div class=rotatedlabel>Context:</div>
            ''')
        for i,context in enumerate(klusDB.contexten(), start=97):
            onFlag = " on" * (chr(i) not in contextStr)
            yield dedent(f'''\
                <button type=button class="togglebutton{onFlag}"
                 onclick="this.classList.toggle('on');doFilter('{currentPage}')"
                 ondblclick="selectUnique(this);doFilter('{currentPage}')"
                 name="context{chr(i)}">
                {escape(context)}</button>
                ''')
        yield dedent('''\
            </div><div id=doelbuttons>
            <div class=rotatedlabel>Doel:</div>
            ''')
        for i,doel in enumerate(klusDB.doelen(), start=97):
            onFlag = " on" * (chr(i) not in doelStr)
            yield dedent(f'''\
                <button type=button class="togglebutton{onFlag}"
                 onclick="this.classList.toggle('on');doFilter('{currentPage}')"
                 ondblclick="selectUnique(this);doFilter('{currentPage}')"
                 name="doel{chr(i)}">
                {escape(doel)}</button>
                ''')
        yield '</div>'
    yield '</div></div></nav>'

# this includes menu open close code
# the first item in the menu opens or closes
# the first <div> in the menu
script = dedent('''\
	 // compute suppressed code voor context or doel
  function getSuppressed(category) {
    var suppressed = [];
    var allSuppressed = true;
    let buttons = document.getElementById(category + "buttons")
        .getElementsByTagName("BUTTON");
    var i;
    for (i = 0; i < buttons.length; i++) {
      let catCode = buttons[i].name[category.length];
      if (!buttons[i].classList.contains("on")) {
        suppressed.push(catCode);
      } else {
        allSuppressed = false;
    } }
    if (allSuppressed) return [];
    return suppressed;
  }

  // show/hide appropriate table elements
  function doFilter(page) {
    let input = document.getElementById('pattern');
    let searchStr = input.value.toUpperCase();
    let contexts = getSuppressed("context");
    let goals = getSuppressed("doel");
    document.getElementById("suppressed").value =
        contexts.join("") +";"+ goals.join("");
    let rows = document.getElementById("tasktable").rows;
    var count = 0;
    let relBound = Number(document.getElementById("minrelevance").value);
    if (page == "Alles") relBound = -1000.;
    var i;
    for (i = 1; i < rows.length; i++) {
      let row = rows[i];
      let tds = row.getElementsByTagName("td");
      let txtValue = tds[2].textContent.toUpperCase();
      let code = tds[0].textContent;
      let relTxt = tds[4].textContent;
      let relValue = relTxt.endsWith("%") ? Number(relTxt.slice(0,-1)) : 100;
      if (txtValue.indexOf(searchStr) >= 0
      && !contexts.includes(code[0])
          && !goals.includes(code[1])
          && relValue >= relBound ) {
        row.style.display = "";
        count += 1;
      } else {
        row.style.display = "none";
      }
    }
    document.getElementById('taskcount').innerHTML =
    count + "/" + (rows.length-1);
  }

  // Double click action: select just this goal/context
  function selectUnique(btn) {
    var i;
    let siblings = btn.parentNode.children;
    // skip label
    for (i = 1; i < siblings.length; i++) {
      let sibling = siblings[i];
      sibling.className = sibling == btn
                        ? "togglebutton on"
                        : "togglebutton";
  } }

  // open and close menus:
  //   first element of menu is open/close button
  //   first div is menu contents to be hidden
  // at onclick, you know this is done already
  window.onmouseup = function(event) {
    let target = event.target;
    let menus = document.getElementsByClassName("menu");
    let i;
    for (i = 0; i < menus.length; i++) {
      let menu = menus[i];
      let menuContents = menu.firstElementChild;
      do {
        if (menuContents.tagName == "DIV") break;
        menuContents = menuContents.nextElementSibling;
      } while (menuContents !== null);
      if (menuContents === null) continue;
      if (menu.contains(target)) {
        if (menu.firstElementChild.contains(target)) {
          if (menuContents.classList.contains('show')) {
            menuContents.classList.remove('show');
            menuContents.style.maxHeight = 0;
          } else {
            menuContents.classList.add('show');
            menuContents.style.maxHeight = menuContents.scrollHeight + 'px';
          }
        }
      } else {
        menuContents.style.maxHeight = null;
        menuContents.classList.remove('show');
  } } }
  ''')
