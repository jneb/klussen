#!python3
"""Jurjen's planning program
list of all tasks
"""

# intro and auxiliary routines
from datetime import datetime, timedelta
from klusdb import KlusInfo
from html import escape
from collections import defaultdict
from general import PROGRAMICONS, dedent
from urllib.parse import quote_plus
import logging

CATEGORIES = [
    "fence",
    "checklist",
    "error",
    "door-closed",
    "open-door",
    "sleeping-in-bed",
    "ghost",
    "refresh",
    "construction",
    "checked",
    "hippopotamus"]
    
def pickIcon(klus, klusInfo, start, deadline, unblocker):
    nextMonth = klusInfo.now + timedelta(days=30)

    # compute icon
    if klusInfo.waiting(klus):
        return 0
    elif klus.isPlanned():
        return 1
    elif not klus.gedaan and deadline and deadline < klusInfo.now:
        return 2
    elif klus.eerst:
        return 3
    elif unblocker:
        return 4
    elif start and start >= nextMonth:
        return 5
    elif klus.context in klusInfo.ctxTimes:
        return 6
    elif klus.herhaal:
        return 7
    elif start and start >= klusInfo.now:
        return 8
    elif klus.gedaan:
        return 9
    else:
        return 10

def listItem(naam, ID, klus, klusInfo, unblockers):
    # compute relevant properties
    if klus.herhaal:
        if klus.gedaan:
            start = klus.gedaan + timedelta(days=klus.min)
            deadline = klus.gedaan + timedelta(days=klus.max)
        else:
            start = deadline = klusInfo.now
    else:
        start = klus.vanaf
        deadline = klus.tot
    icon = pickIcon(klus, klusInfo, start, deadline, ID in unblockers)

    # format properties
    code = klusInfo.filterCode(klus)

    rel = klusInfo.relevantie(klus)
    relkey = format(max(0, rel+10), "7.3f")
    if rel < -1: relevance = '--'
    elif rel > 2: relevance = '++'
    else: relevance = format(rel, '03.0%')

    if isinstance(deadline, datetime):
        startkey = format(start, '%y%m%d%H%M')
        deadlinekey = format(deadline, '%y%m%d%H%M')
        if abs(deadline - datetime.now()) < timedelta(days=91):
            start = format(start, '%m-%d@%H')
            deadline = format(deadline, '%m-%d@%H')
        else:
            start = format(start, '%y-%m-%d')
            deadline = format(deadline, '%y-%m-%d')
    else:
        startkey = deadlinekey = "9"
        start = deadline = '-'

    return dedent(f'''\
        <tr>
          <td class=key>{code}</td>
          <td><img src="/img/{escape(CATEGORIES[icon])}.png" height=20px></td>
          <td><a href="/edit/{ID}?returnto={quote_plus("/alles")}">{escape(naam or "(Leeg)")}</a></td>
          <td><img src="/img/{klus.icon}.png" height=20 loading=lazy></td>
          <td class="numcol ifwide">{relevance}</td>
          <td class=ifwide>{start}</td>
          <td>{deadline}</td>
          <td class=key>{relkey}</td>
          <td class=key>{startkey}</td>
          <td class=key>{deadlinekey}</td>
          <td class=key>{icon}</td>
          <td class=key>{klus.icon}</td>
        </tr>
        ''')

# standard routines and data
style = dedent('''\
    #tasktable th button {
      padding: 8px 25px 8px 0;
      background-color: HoneyDew;
      background-image: url(/img/sorting-arrows.png);
      background-position: right;
      background-size: 25px 25px;
      background-repeat: no-repeat;
      border-radius: 4px;
      cursor: pointer }
    #tasktable tr .key { display: none }
    @media only screen and (max-width: 500px) {
      .ifwide { display: none} }
    ''')


def body(klusDB):
    """Body code.
    Variable.
    """
    if not set(CATEGORIES) <= PROGRAMICONS:
        logging.error("Icons missing in PROGRAMICONS: %s",
            set(CATEGORIES) - PROGRAMICONS)
    yield dedent('''\
        <p id=explain>Meest recent gewijzigde klussen staan bovenaan.</p>
        <table id=tasktable>
        <thead><tr>
          <th><button type=button onclick="sortTable(10)">
           &nbsp;</button></th>
          <th><button type=button onclick="sortTable(2)">
            Omschrijving</button></th>
          <th><button type=button onclick="sortTable(11)">
           &nbsp;</button></th>
          <th class=ifwide><button type=button onclick="sortTable(7)">
            %</button></th>
          <th class=ifwide style="width:5em"><button type=button onclick="sortTable(8)">
            Van</button></th>
          <th style="width:5em"><button type=button onclick="sortTable(9)">
            Tot</button></th>
        </tr></thead><tbody>
        ''')
    klusList = [
        (klus.naam, ID, klus)
        for ID,klus in klusDB.items()
        if not klus.verwijderd
        ]
    klusInfo = KlusInfo(klusDB)
    # shove ctxTimes in klusInfo saving a parameter
    klusInfo.ctxTimes = klusDB.ctxTimes
    klusInfo.now = datetime.now()
    unblockers = {
        klus.eerst
        for klus in klusDB.values()
        if not klus.verwijderd
        } - {None, ""}
    for naam,ID,klus in reversed(klusList):
        yield listItem(naam, ID, klus, klusInfo, unblockers)
    yield dedent('''\
        </tbody></table>
        <h3>Legenda</h3>
        Het icon geeft het eerste weer
        van de volgende eigenschappen van een klus:
        <table>
        ''')
    for i,explanation in enumerate(dedent('''\
        Klus verschijnt niet omdat hij wacht
        Klus is ingepland, dus relevantie wijkt af
        Deadline is verlopen
        Klus ingesteld om te wachten op andere klus
        Andere klus wacht mogelijk op deze
        Komende maand niet relevant
        Context van klus wordt mogelijk automatisch verborgen
        Herhalende klus
        Nu relevant
        Klus is af'''
            ).splitlines()):
        yield dedent(f'''\
            <tr><td><img src="img/{CATEGORIES[i]}.png" height=30>
            </td><td>{explanation}
            </td></tr>
            ''')
    yield '</table>'


# adapted from https://www.w3schools.com/howto/howto_js_sort_table.asp
# removed useless variables
# removed useless lines
# use textContent instead of innerHTML
# binary insertion instead of the horrible bubble sort!
# now does O(n*log(n)) compares and <n inserts

script = dedent('''\
    function sortTable(n) {
      let rows = document.getElementById("tasktable").rows;
      function getData(i) {
        return rows[i].getElementsByTagName("TD")[n].textContent.toLowerCase();
      }
      var reverse = false;
      while (true) {
        var switchcount = 0;
        var i, x, y;
        for (i = 1; i < (rows.length - 1); i++) {
          x = getData(i);
          y = getData(i+1);
          if (reverse ? (x < y) : (x > y)) {
            var lo = 1, hi = i + 1;
            while (hi - lo > 1) {
              let mid = lo + hi >> 1;
              x = getData(mid - 1);
              if (reverse ? (x < y) : (x > y)) {
                hi = mid;
              } else {
                lo = mid;
            } }
            rows[i].parentNode.insertBefore(rows[i + 1], rows[lo]);
            switchcount ++;
        } }
        if (switchcount || reverse) break;
        reverse = true;
      }
      document.getElementById("explain").innerHTML = "";
    }
    ''')

def post(forms, klusDB, klusID):
    """Process post data
    """
    return "/alles"
