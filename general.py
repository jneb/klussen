#!python3
"""
This program implements Jurjen's version
of Life Balance
This file contains various little objects
for general use.
"""

import os
import io
from datetime import timedelta, datetime
from pathlib import Path
from threading import Lock
import logging
from operator import methodcaller

fileRoot = Path(__file__).parent

# program constants
# location of icons
ICONZIP = "img/icons.zip"
VERSION = "2.404"
VERSIONSTRING = f"Klussen {VERSION} door Jurjen N.E. Bos"

# names used by program
WORKCONTEXT, WORKGOAL = "Werk", "Productief"
EVENING = "Avond"
EASYCONTEXT, HARDCONTEXT = "Klusje", "Grote klus"
ORGANISED = "Georganiseerd"

# default relevance limit
MINRELEVANCE = 33
# maximum number of fetched icons
FETCHAMOUNT = 80

# planning constants
# minimum allowed period between start and end in hours
MINHOURS = 6
# period around planning moment
PLANPERIOD = timedelta(hours=12)

# line length in database (optimised for mobile screen)
DBWIDTH = 43

PROGRAMICONS = {
   'search', 'circled-thin', 'fence',
   'error', 'door-closed', 'open-door',
   'sleeping-in-bed', 'refresh', 'ghost',
   'construction', 'checked', 'hippopotamus',
   'checklist', 'sorting-arrows',
   }

# make task showing each new icon
MAKEICONTASK = False

openQuote = '\N{left double quotation mark}'
closeQuote = '\N{right double quotation mark}'

# log level to don't get server debug stuff
# but include cache debug
HTTP = 12

# log in a way that is visible on qpython
def makeOption(options, current=""):
    """Make options in html
    and select the right one
    ends with /select
    """
    for o in options:
        s = ' selected' * (o == current)
        yield f'  <option value="{o}"{s}>{o}</option>\n'
    yield ' </select>'

def getLocalIP():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("192.168.2.1", 80))
    result = s.getsockname()[0]
    s.close()
    return result

def lineGrouper(lines, indent=""):
    """group lines into one line with
    given indent (or less),
    followed by lines with deeper indent
    each group is an iterator
    """
    deepIndent = indent + " "
    # fill line with first noncomment line
    line = next(lines)
    while line.isspace() or line.startswith('#'):
        line = next(lines)
    def group():
        """generator for groups
        first line of group is already in line
        followed by lines of deeper indent
        """
        nonlocal line
        yield line
        for line in lines:
            if line.startswith('#'):
                continue
            if not line.startswith(deepIndent):
                return
            yield line
        # no more groups
        line = None
    while line:
        yield group()

def dataURI(data, mimetype='svg'):
    """Make a data URI that generates a file
    with given mimetype
    use in url() or src=
    form:
        data:[<mediatype>][;base64],<data>
    """
    # handle shorthand
    mimetype = {
        'svg': 'image/svg+xml',
        'ecma': 'text/javascript',
        }.get(mimetype, mimetype)
    data = data.translate(str.maketrans({
        '\n': ' ',
        '"': "'",
        '#': "%23",
        '/': "%2f",
        '&': "%26",
        }))
    return f'data:{mimetype},' + data
    # this works, but produces too much data
    return f'data:{mimetype};base64,' + b2a_base64(data.encode())[:-1].decode()

def dedent(s):
    """Simplified fast version of textwrap.dedent
    that just removes all indent
    """
    return '\n'.join(map(methodcaller('lstrip'), s.splitlines()))

def generatorGzipper(gen, compresslevel=4, wsize=15):
    """From a bytes generator,
    generate bytes objects that are
    the gzipped equivalent
    compresslevel=0(fast) to 9(small)
    wsize=9(early output, but slow) to 16(good compression)
    expect your output after about 40K of data
    fastest compression, 10% from optimal:
      4,15 (2,5 times faster than smallest)
    """
    import zlib
    compressor = zlib.compressobj(
        level=compresslevel,
        wbits=16+wsize,
        strategy=zlib.Z_DEFAULT_STRATEGY)
    for data in gen:
        outData = compressor.compress(data)
        if outData:
            yield outData
    # don't forget the end
    yield compressor.flush()

class RWlock:
    """Allow multiple readers to simultaneously read
    a resource without delay
    Allows for RW and RCU
    use like:
    rLock,cLock,uLock = RWlock.makeRCUlocks()
    def reader():
        with rLock: read
    def writer ():
        with cLock:
            copy
            with uLock: replace
    Objects of this class are the Rlock,
    the cLock and uLock ate attributes
    Note: writer starvation in not perverted,
    that isn't needed here
    """
    def __init__(self):
        self.rLock = Lock()
        self.wLock = Lock()
        self.readers = 0

    def __enter__(self):
        with self.rLock:
            if not self.readers:
                self.wLock.acquire()
            else:
                logging.log(HTTP, "Number of readers: %s", self.readers)
            self.readers += 1
                
    def __exit__(self, type, value, tb):
        with self.rLock:
            self.readers -= 1
            if not self.readers:
                self.wLock.release()

    @classmethod
    def makeRUlocks(cls):
        rLock = cls()
        return rLock, rLock.wLock

    @classmethod
    def makeRCUlocks(cls):
        rLock = cls()
        return rLock, Lock(), rLock.wLock

# for debug: short stack summary
def stackdump():
    import traceback, os.path
    return '\n'.join(
        f"   {f.name} at {os.path.basename(f.filename)}:{f.lineno}"
        for f in traceback.extract_stack(limit=5)[:-1]
        )


def main(level=6,wsize=11):
    f = Path('styles.txt')
    pos = 0
    def readf():
        nonlocal pos
        with f.open('rb') as fo:
            while True:
                line = fo.read(1<<14)
                if not line: break
                pos += len(line)
                yield line
    g = generatorGzipper(
        readf(),
        level,
        wsize)
    from binascii import hexlify
    size = 0
    for d in g:
        size += len(d)
        #print(pos, size, hexlify(d[:15]).decode())
    return size

if __name__ == "__main__":
    result = [(main(l,w),l,w)
        for l in range(10)
        for w in range(9,16)]
    result.sort()
    from time import perf_counter
    for r,l,w in result:
        t = perf_counter()
        main(l,w)
        print(r,round(perf_counter()-t,3),l,w)
