#!python3
"""Jurjen's planning program
klaar page: achievements
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
from datetime import date, timedelta
from general import dedent
from collections import defaultdict

def listItem(target, doelen, totalenergy, scale, counts):
    weekend = ' class=yellow' * (target.weekday() > 4)
    yield f'<tr{weekend}><td>{target:%d %b}</td>'
    for doel in doelen:
        t = counts[target, doel]
        if t < 2: t = ''
        yield dedent(f'''\
            <td>
            <meter value="{totalenergy[target, doel] / scale:.2f}"></meter>
            <span>{t}</span>
            </td>
            ''')
    yield '</tr>'


# standard routines
style = dedent('''\
    #klaar {
      table-layout: fixed }
    #klaar td:first-child {
      align-text: right;
      width: 4em;
      font-size: var(--small) }
    #klaar thead th:not(:first-child) {
      width: 40px;
      height: 80px }
    #klaar td:not(:first-child) { position: relative }
    #klaar meter { width: 40px }
    #klaar span {
      position: absolute;
      right: 5px;
      z-index: 1  }
    ''')


def body(klusDB):
    """Body code.
    Variable; klusID is ignored
    """
    history = 31
    doelen = klusDB.doelen()
    yield '<table id=klaar><thead><tr><th>Datum</th>'
    for doel in doelen:
        yield f'<th><div class=rotatedlabel>{doel}</div></th>'
    yield '</tr></thead><tbody>'
    # get data
    totalenergy = defaultdict(int)
    counts = defaultdict(int)
    for klus in klusDB.values():
        for dt in klus.alleGedaan():
            totalenergy[dt.date(), klus.doel] += klus.energie
            counts[dt.date(), klus.doel] += 1
    today = date.today()
    scale = max(totalenergy.values())
    for days in range(history):
        target = today - timedelta(days=days)
        yield from listItem(target, doelen, totalenergy, scale, counts)
    yield '</tbody></table>'


script = ""

def post(forms, klusDB, klusID):
    """Process post data
    only handles tags defined here
    """
