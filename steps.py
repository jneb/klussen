"""Substep editor for complicated tasks.
The only part in the program where I use external libraries.

"""

from general import dataURI, dedent, openQuote, closeQuote
from html import escape
import logging

style = dedent('''\
    #steps {
      width: 100%;
      background-color: LightYellow;
       }
    #steps input[type=text] {
      font-size: --entryfs;
      width: 100% }
    #steps td img {
      margin: -2px;
      cursor: n-resize }
    .placeholder { background-color: LightGrey }
    ''')


def body(klusDB, ID, returnto):
    # two libraries are included here
    # add hidden element for serial data
    # and make sure it's filled at submit
    yield dedent(f'''\
        <script src="/inc/DragDropTouch.js"></script>
        <script src="/inc/html5sortable.js"></script>
        <input type=hidden id=stappen name=stappen>
        <script>
          document.getElementById('theform')
            .addEventListener('submit', serialize)
        </script>
        <h2>
        Tussenstappen voor {openQuote}{escape(klusDB[ID].naam)}{closeQuote}</h2>
        <table id=steps onclick="emphasizeSend()">
        <tbody class=sortable>
        ''')
    for stap,flag in (klusDB[ID].stappen or {}).items():
        yield dedent(f'''\
            <tr><td>
            <input type=checkbox {"checked" * flag} tabindex=-1>
            </td><td>
            <input type=text value="{escape(stap)}">
            </td><td>
            <img class=handle src="/img/sorting-arrows.png" height=30>
            </td></tr>
            ''')
    # add item to allow adding new items
    yield dedent('''\
        <tr><td>
        <input type=checkbox title=OK tabindex=-1>
        </td><td>
        <input type=text placeholder=Nieuw
           onkeyup="addentry(this)"></input>
        </td><td>
        <img class=handle src="/img/sorting-arrows.png" height=30>
        </td></tr></tbody></table>
        ''')
    # setup sortable class. needs to be activated after loading
    setupCode = dedent('''\
        sortable('.sortable', {
          forcePlaceholderSize: true,
          handle: '.handle',
          placeholder: '<tr class=placeholder><td colspan=3>',
          itemSerializer: function (item, container) {
              let checked = item.node.children[0].firstElementChild.checked;
              let value = item.node.children[1].firstElementChild.value;
              return (checked ? "*" : "_") + value;
            },
        })
        ''')
    yield f'''<script src="{dataURI(setupCode, 'ecma')}"></script>'''


script = dedent('''\
    // add entry to sortable list
    function addentry(item) {
      let theRow = item.parentElement.parentElement;
      let theTable = theRow.parentElement;
      let lastRow = theTable.lastElementChild;
      // skip sortable stuff to find last item
      while (lastRow.tagName != "TR") lastRow = lastRow.previousElementSibling;
      let contents = lastRow.children[1].firstElementChild.value;
      if (contents) {
        let newNode = theRow.cloneNode(true);
        newNode.children[0].firstElementChild.checked = false;
        newNode.children[1].firstElementChild.value = "";
        theTable.appendChild(newNode);
        sortable('.sortable');
    } }
    function serialize() {
      document.getElementById('stappen').value =
        sortable('.sortable', 'serialize')
          [0].items.join('\\n');
    }
    ''')


def post(forms, klusDB, klusID):
    if 'stappen' in forms:
        # use specific assignment; from post is for edit
        klus = klusDB[klusID]
        klus.stappen = klus.fields['stappen'].fromSerial(forms.stappen)
        klusDB.journal(klusID)
    return f'/edit/{klusID}'
