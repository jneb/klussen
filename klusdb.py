#!python3
"""Database for klussen
The database consist of Entries
an entry is a key/value pair
a key is a string
a value may be a subentry
an entry has an indent:
    main entries have indent 0
    subentries have an indent of one more than their parent
External
entry (or subentry) is:
    - indent * 2 spaces
    - key
    - colon (:)
    - for short entries: a space  followed by the value
    - for long entries: (optional specifier char) and a newline,
      followed by lines starting with more than indent*2 spaces
A value is specific for its type, determined by ID:
    - of the whole entrysee is omitted, there's a default
    - integers: in decimal
    - float: decimal, with as few digits as possible
    - string: can be in the forms
        short: just the string
        |: multiline, each line indented
        >: long, wrapped and indented
    - subentry: can be in the forms
        short: {key: value; key: value}
        long: like entry, with one more indent

internally, we have Value and Entry classes
Value:
    subclass of relevant python type
    attribute:
    indent
    conversion functions:
    toYAML: everything (after the ID:) as string
        both short and long forms
        with extra argument to decide whether to use short form
    fromYAML: create from YAML (without ID:)
    toHTML: for use in HTML input
    fromHTML: for use in HTML input

Entry is a value with a default

Record is a value with subentries, it is also a dict
    and methods:
    valueClass(ID): compute class of subentry
Klus is a subclass of DictEntry

KlusDB is a dict of Entries

@@ next steps:
clean up Attr classes
test again
incorporate in production
"""

from pathlib import Path
from datetime import datetime, timedelta
from klus import Klus, Record, StrAttr
from general import (
    lineGrouper, ICONZIP, RWlock,
    WORKCONTEXT, WORKGOAL, ORGANISED, EVENING, EASYCONTEXT, HARDCONTEXT,
    MINRELEVANCE)
from random import randrange
import zipfile
import email.utils
import os
from threading import Lock
import logging
from collections import OrderedDict

me = Path(__file__)


class Priorities(list, Record):
    """A list of numbers
    Only short form, no brackets needed
    Short form: 3,4,5
    """
    def __init__(self):
        self.d = []

    @classmethod
    def fromYAML(cls, firstLine, remainder):
        result = cls()
        # short form assumed
        result.d.extend(map(int, firstLine.split(',')))
        return result

    def convert(self, klusDB):
        """Add information about klusDB so that
        it can be used as a dictionary doel->int
        """
        doelen = klusDB.doelen()
        if len(doelen) != len(self.d):
            logging.warning("Couldn't set priorities: %s", self.d)
            self.d = {}
        else:
            self.d = dict(zip(doelen, self.d))

    # these routines only work after conversion
    def __getitem__(self, index):
        return self.d.get(index, 0)

    def __setitem__(self, index, value):
        self.d[index] = value

    def __bool__(self):
        return any(self.d.values())

    def dump(self, *, klusDB):
        """Make an entry that matches doelen
        in klusDB (after conversion)
        """
        return ', '.join(
            str(self[doel])
            for doel in klusDB.doelen()
            )


class SuppressTimes(dict, Record):
    """Mapping from strings to
    a subset of hours in the week
    (determined by hour and day of week)
    values are pairs of allowed days, allowed hours
    Also determines if "now" is in the period
    Syntax, e.g. ma,di,do,vr@5-15
    In short form {ctx:...; ctx:...}
    """
    weekday = list("ma di wo do vr za zo".split())
    dayNumber = {n:i for i,n in enumerate(weekday)}

    @classmethod
    def fromYAML(cls, firstLine, remainder):
        if firstLine:
            # short form: split into entries
            if not (firstLine.startswith('{') and firstLine.endswith('}')):
                raise SyntaxError
            remainder = firstLine[1:-1].split(';')
        result = dict.__new__(cls)
        for item in remainder:
            context, sep, value = item.partition(':')
            if not sep: raise SyntaxError
            result[context.lstrip()] = result.parseSubset(value)
        return result

    def parseSubset(self, subset):
        """parse subset definition
        >>> SuppressTimes().parseSubset('ma-wo,vr@3-8')
        ([0,1,2,4], range(3, 8))
        >>> SuppressTimes().parseSubset('3-')
        (range(7), range(3, 24))
        """
        days, sep, times = subset.partition('@')
        if sep:
            return self.parseDays(days), self.parseTimes(times)
        return range(7), self.parseTimes(days)

    def parseDays(self, days):
        """parse days definition
        >>> SuppressTimes().parseDays('ma-wo,vr')
        [0,1,2,4]
        """
        result = []
        for dayFromTo in days.split(','):
            start,sep,stop = dayFromTo.partition('-')
            if sep:
                result.extend(range(self.dayNumber[start.strip()], self.dayNumber[stop.strip()] + 1))
            else:
                result.append(self.dayNumber[start.strip()])
        return result

    def parseTimes(self, times):
        """Parse times definition
        >>> SuppressTimes().parseSubset('3-')
        range(3, 24)
        """
        start,sep,stop = times.partition('-')
        if not sep:
            raise SyntaxError(times)
        return range(int(start.strip() or 0), int(stop.strip() or 24))

    def dump(self):
        """Make an entry for klusDB
        """
        return '\n  '.join(
            self.dumpSubset(context)
            for context in self
            )

    def dumpSubset(self, context):
        # build list of from,to pairs
        dayList = []
        for day in self[context][0]:
            if dayList and dayList[-1][1] == day - 1:
                dayList[-1][1] += 1
            else:
                dayList.append([day, day])
        # build days string from pieces
        dayStr = []
        if dayList != [[0,6]]:
            for lo,hi in dayList:
                dayStr.append(self.weekday[lo])
                if lo < hi: 
                    dayStr[-1] += '-' + self.weekday[hi]
        r = self[context][1]
        startStr = str(r.start) if r.start else ''
        endStr = str(r.stop) if r.stop < 24 else ''
        return (
            context + ': '
            + ','.join(dayStr)
            + '@' * bool(dayStr)
            + startStr
            + '-' + endStr)

    def toSuppress(self, now=datetime.now()):
        """Find names of contexts to suppress now
        """
        dow = now.date().weekday()
        hour = now.time().hour
        return [context
                for context,(days,hours) in self.items()
                if dow not in days or hour not in hours]


class KlusDB(OrderedDict):    
    def __init__(self, path:Path):
        self.path = path
        super().__init__()
        self.parseFile(path)
        self.ctxTimes = self.get('contexttijden') or SuppressTimes()
        if 'prioriteiten' in self:
            self.priorities = self['prioriteiten']
            del self['prioriteiten']
        else:
            self.priorities = Priorities()
        self.priorities.convert(self)
        if 'contexttijden' in self:
            del self['contexttijden']
        # for manual entry
        self.journalFile = None
        self.lock = Lock()
        self.fixIDs()
        # initial values for main program
        self.suppressed = self.defaultSuppressed()
        self.minrelevance = MINRELEVANCE
        self.deleted = None
        self.timestamp = datetime.fromtimestamp(os.path.getmtime(path))
        # keep icon zip open
        self.iconzip = zipfile.ZipFile(ICONZIP)
        self.ziprLock, self.zipcLock, self.zipuLock = RWlock.makeRCUlocks()

    def defaultSuppressed(self):
        suppress = self.ctxTimes.toSuppress()
        return ''.join(
            chr(c)
            for c,ctxt in enumerate(self.contexten(), start=97)
            if ctxt in suppress) + ';'

    def doelen(self):
        """Doelen, works even before prioriteiten and contexttijden
        are removed
        """
        return sorted(set((WORKGOAL, ORGANISED)).union(
            r.doel
            for id,r in self.items()
            if len(id) <= 2 and r.doel is not None
            ) - {""})

    def contexten(self):
        return sorted(set((WORKCONTEXT, EVENING, EASYCONTEXT, HARDCONTEXT)).union(
            r.context
            for r in self.values()
            if r.context is not None
            ) - {""})

    def newID(self):
        """make fresh ID for nee klus
        """
        if len(self) > 900:
            raise OverflowError("DB full")
        while True:
            a,b = divmod(randrange(26 * 36), 36)
            IDa = chr(65 + a)
            IDb = str(b) if b < 10 else chr(55 + b)
            ID = IDa + IDb
            if ID not in self:
                return ID

    def after(self, recID):
        """genererate what is allowed after this
        """
        for ID,r in self.items():
            if recID in r.eerst:
                yield ID

    # parser routines
    def lineTracker(self, path):
        """Keep line number and line for proper error message
        """
        for self.lineno, self.lastline in enumerate(path.open(), start=1):
            yield self.lastline
        del self.lineno, self.lastline

    def parseFile(self, path):
        self.entryCounter = 0
        if not path.exists():
            return
        for group in lineGrouper(self.lineTracker(path), ""):
            self.entryCounter += 1
            self.parseRecord(group)            

    def parseRecord(self, group):
        """Parse line group iterator
        type is determined by ID
        parser for type gets ID, rest of first line
        and an iterator for more lines
        """
        line = next(group)
        ID, sep, value = line.partition(':')
        if not sep:
            raise SyntaxError(f'line {self.lineno}: colon expected in "{line}"')
        try:
            newentry = self.getParser(ID)(value.strip(), group)
            if ID in self:
                newentry.replacesEntry(self[ID])
            self[ID] = newentry
            self.move_to_end(ID)
        except SyntaxError:
            raise SyntaxError(f'line {self.lineno}: Error in record')
        # check if record parser used up iterator
        for line in group:
            raise SyntaxError(f'line {self.lineno}: unexpected indented line "{line}"')

    def getParser(self, ID):
        """Determine parser for this record ID
        """
        if 0 < len(ID) <= 2:
            return Klus.fromYAML
        if ID == "prioriteiten" or ID == "priorities":
            return Priorities.fromYAML
        if ID == "contexttijden" or ID == "contextvalid":
            return SuppressTimes.fromYAML
        raise SyntaxError(f'line {self.lineno}: Unknown ID: {ID}')

    def fixIDs(self):
        """allow *<letter> for ID
        for manual entry
        rewrites DB if needed
        """
        rewriteflag = False
        for ID in list(self):
            # make sure journal works
            if ID.startswith('*'):
                self[self.newID()] = self[ID]
                del self[ID]
                rewriteflag = True
        if rewriteflag:
            self.rewrite()

    # writer
    def writeHeader(self, f=None):
        """write goals and contexts
        if f omitted, save in Db file
        (creating journal if needed)
        """
        if f is None:
            self.journal()
            f = self.journalFile
        print('#doelen:', ','.join(self.doelen()), file=f)
        print('#contexten:', ','.join(self.contexten()), file=f)
        if self.priorities:
            print("prioriteiten:", self.priorities.dump(klusDB=self), file=f)
        if self.ctxTimes:
            print("contexttijden:", self.ctxTimes.dump(), sep='\n  ', file=f)

    def rewrite(self, maxAge=None):
        """Make new database with one repetition for each task
        optionally removing older than maxage days
        """
        if maxAge is not None:
            removeBefore = datetime.now()-timedelta(days=maxAge)
        if self.journalFile: self.journalFile.close()
        self.journalFile = None
        bak = self.path.with_suffix('.bak')
        if self.path.exists():
            self.path.replace(bak)
        with self.path.open('w') as f:
            self.writeHeader(f)
            self.entryCounter = 2
            for recID, klus in self.items():
                if klus.verwijderd: continue
                if (maxAge is not None
                        and not klus.herhaal
                        and klus.gedaan
                        and klus.gedaan < removeBefore):
                    klus.verwijderd = True
                    continue
                for dt in klus.eerderGedaan:
                    if maxAge is not None and dt < removeBefore:
                        continue
                    subKlus = Klus()
                    subKlus.gedaan = dt
                    print(recID + subKlus.dump(), file=f)
                    self.entryCounter += 1
                print(recID + klus.dump(), file=f)
                self.entryCounter += 1
        self.flush()

    # journaling
    def journal(self, recID=None):
        """create journal file if needed
        and write klus to disk if given
        """
        if not self.journalFile:
            self.journalFile = open(self.path, 'a')
            print(f"#date: {datetime.now():%y-%m-%d@%H:%M:%S}", file=self.journalFile)
        if recID:
            logging.info("updating %s: %s", recID, self[recID].naam)
            print(recID + self[recID].dump(), file=self.journalFile)
            self.entryCounter += 1
        self.flush()

    def flush(self):
        """Update internal and external timestamp
        and save file changes
        """
        with self.lock:
            if self.journalFile:
                self.journalFile.flush()
            else:
                self.path.touch()
            self.timestamp = email.utils.localtime()
        from general import stackdump, HTTP
        logging.debug(stackdump())
        logging.info("new DB timestamp: %s", self.timestamp)

class KlusInfo:
    """For categorising klussen.
    Used by nu and alles
    Usage KlusInfo(klusDB)(klus)
    Computes a code based on context and goal,
    and the adjusted relevance.
    Adjustment factor exp(+-2/3)= 0.51..1.05
    Code consists of <context letter><goal letter><herhaal digit>
    """
    def __init__(self, klusDB):
        from math import exp
        self.klusDB = klusDB
        self.contexten = {
            context: chr(n)
            for n,context in enumerate(klusDB.contexten(), start=97)
            }
        self.doelen = {
            doel: chr(n)
            for n,doel in enumerate(klusDB.doelen(), start=97)
            }
        self.factors = {
            goal: exp(klusDB.priorities[goal] / 6)
            for goal in self.doelen}

    def relevantie(self, klus):
        """adjusted relevance
        """
        return klus.relevantie() * self.factors[klus.doel]

    def filterCode(self, klus):
        return self.contexten[klus.context] + self.doelen[klus.doel]

    def waiting(self, klus):
        """Check if task waits for another task
        """
        if not klus.eerst: return False
        if klus.eerst not in self.klusDB: return False
        return not self.klusDB[klus.eerst].finished()


if __name__ == "__main__":
    import pdb, difflib, general
    from datetime import timedelta
    os.chdir(me.parent)
    pdb.set_trace()
    try:
        klussen = me.parent / 'klussen.db'
        path = me.parent / 'test.db'
        path.write_text(klussen.read_text())
        db = KlusDB(path)
        newID = db.newID()
        db[newID] = Klus("Kort")
        db.journal(newID)
        db[newID].addGedaan(datetime(2021,9,14,22))
        db.journal(newID)
        db[newID].addGedaan(datetime(2021,9,14,21))
        db.journal(newID)
        db[newID].addGedaan(datetime(2021,9,15,22))
        db.journal(newID)
        print(*db[newID].alleGedaan(), sep=", ")
        k = Klus("Testen")
        k.min = 5
        k.max = 12
        k.gedaan = datetime.now()
        k.notitie = "Regel 1\nRegel 2"
        k.herhaal = True
        newID = db.newID()
        db[newID] = k
        db.journal(newID)
        db.journalFile.close()
        print("Journal:")
        for line in difflib.unified_diff(
                klussen.read_text().splitlines(),
                path.read_text().splitlines()):
            print(line)
        db.rewrite()
        print("Rewrite:")
        for line in difflib.unified_diff(
                path.with_suffix('.bak').read_text().splitlines(),
                path.read_text().splitlines()):
            print(line)
        path2 = me.parent / 'test2.db'
        path2.write_text(path.read_text())
        db2 = KlusDB(path)
        db2.journalFile = open(klussen)
        db2.rewrite(31)
        print("Rewrite one month:")
        for line in difflib.unified_diff(
                path.with_suffix('.bak').read_text().splitlines(),
                path.read_text().splitlines()):
            print(line)

    except Exception as e:
        print("Sorry:", e)
        pdb.post_mortem()
