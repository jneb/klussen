#!python3
"""Jurjen's planning program
klus editor
this file has some modifications for testing in qpython
Pagina ziet er ongeveer zo uit:
Beschrijving (groot tekstvak)
Levensdoel (popup)  Context (popup)
Icon (???)          Tijd/energie (slider)
Gedaan op:          Eerst (popup)

Herhalend (checkbox), kiest uit
[Ja]               [Nee]
Minimum # dagen    Vanaf:
Maximum # dagen    Uiterlijk:
Gepland op:

Relevantie (wordt live uitgerekend)
Lijst subklussen: (links, wordt aangegeven als die er zijn)
Notities: vrije tekst

Alles met : is een datum
"""

from html import escape
from urllib.parse import quote_plus
from datetime import timedelta
from math import exp
import logging
import zipfile

# intro and auxiliary routines
import klusdb
from general import (makeOption, dedent, openQuote, closeQuote,
    MINHOURS, WORKCONTEXT, WORKGOAL, ORGANISED, EVENING, EASYCONTEXT, HARDCONTEXT)


def iconEntries(klusDB, current):
    """Generate html for icon menu.
    Icon loading is postponed.
    """
    with klusDB.ziprLock:
        namelist = sorted(klusDB.iconzip.namelist())
    for name in namelist:
        if not name.endswith('.png'):
            logging.warning("Invalid icon in zip file: %s", name)
        iconName = name[:-4]
        cls = ' class=selectedicon' * (iconName == current)
        yield dedent(f'''\
            <img{cls} src="/img/{escape(name)}" height=30 width=30
             onclick="iconClicked('{escape(iconName)}')" loading=lazy>
            ''')

def quickMenu():
    return dedent(f'''\
        <div class="menu right">
        <button type=button class=bigbutton>
          Wijzig \N{black right-pointing double triangle}
        </button>
        <div onclick="closeMenu(this)">
          <table id=quickmenu onclick="emphasizeSend()">
          <tr><th>Plan in</th><th>Stel in</th></tr>
          <tr><td>
            <button type=button
             onclick="setTaskPeriod(0,0)">Vandaag</button>
          </td><td>
            <button type=button
                  onclick="setContextGoal('{EASYCONTEXT}', '{ORGANISED}')">Huishoudklusje</button>
          </td></tr><tr><td>
            <button type=button
             onclick="setTaskPeriod(0,1)">Vandaag of morgen</button>
          </td><td>
            <button type=button
             onclick="setContextGoal('{EVENING}', '{ORGANISED}')">Avondklusje</button>
          </td></tr><tr><td>
            <button type=button
             onclick="setTaskPeriod(1,1)">Morgen</button>
          </td><td>
            <button type=button
             onclick="setContextGoal('{WORKCONTEXT}', '{WORKGOAL}')">Werk</button>
          </td></tr><tr><td>
            <button type=button
             onclick="d=(new Date()).getDay();setTaskPeriod(6-d,7-d)">Dit weekend</button>
          </td><th style="border-top:2px solid black">Herhaal
          </th></tr><tr><td>
            <button type=button
             onclick="d=(new Date()).getDay();setTaskPeriod(8-d,12-d)">Volgende week</button>
          </td><td>
            <button type=button
             onclick="setPeriod()">Deze periode</button>
          </td></tr></table>
        </div></div>''')

# standard routines
style = dedent('''\
    #naam {
      font-size: var(--titlefs);
      margin 0 10px;
      padding: 5px 10px;
      border: 2px solid black;
      display: block;
      width: 100% }
    #kluslayout {
      width: 100%;
      table-layout: fixed;
      margin: 0 }
    #kluslayout tr { padding: 5px 0 }
    #kluslayout td { padding: 4px }
    .iconmenu > img {
      display: block;
      margin: auto }
    #iconlist .selectedicon {
      border: 3px solid black;
      border-radius: 5px }
    select {
      width: 8em;
      border: 2px solid Grey; border-radius: 5px;
      padding: 5px 2px;
      text-overflow: ellipsis; white-space: nowrap;
      font-size: var(--small) }
    td select[name=doel] { background-color: MistyRose }
    td select[name=context] { background-color: PaleTurquoise }
    .num { width: 3em }
    .dt { width: 9em }
    .zoekklus { width: 6em }
    #icon { width: 100px }
    #energie { width: 100% }
    .yellow { background-color: LightYellow }
    .menu.right > div,
    .menu.medium > div,
    .menu.middle > div {
      position: absolute;
      left: 0;
      width: 20em }
    .menu.medium > div { width: 12em }
    .menu.middle > div { left: -10em }
    .menu.right > p {
      border: 2px solid Grey;
      border-radius: 5px;
      text-align: center;
      padding: 5px }
    #quickmenu { border-collapse: collapse }
    #quickmenu th { color: black }
    #notitie {
      width: 100%;
      border: 1px solid grey;
      resize: vertical }
    .collapsible {
      border: 2px solid Grey;
      border-radius: 5px;
      background-color: HoneyDew;
      margin: 1em var(--menuheight) }
    .collapsible, ol li { font-size: var(--small) }
    .collapsible div {
      max-height: 0;
      overflow: hidden }
    #subtasks { margin: 3px 0 }
    #subtasks li {margin: 3px }
    #subtasks label { padding-left: 1em }
    #subtasks input {
      position: relative;
      left: -1em }
    .remark {
      color: DarkGrey;
      font-size: var(--small) }
    #taskbuttons button,
    #taskbuttons > a {
      width: 84px;
      border: 1px solid black;
      border-radius: 5px;
      background-color: PaleTurquoise;
      text-decoration: None;
      display: inline-block;
      text-align: center }
    #taskbuttons span:nth-child(1) button {
      background-color: LightYellow }
    #taskbuttons span:nth-child(2) button {
      background-color: Honeydew }
    ''')

def body(klusDB, ID, returnto=""):
    """Body code.
    """
    # name, icon
    klus = klusDB[ID]
    yield dedent(f'''\
        <input type=text name=naam id=naam value="{escape(klus.naam)}"
         onkeyup="emphasizeSend()" required>
        <input type=hidden name=ID value="{ID}">''')
    if returnto:
        yield f'<input type=hidden name=returnto value="{escape(returnto)}">'
    yield dedent(f'''\
        <table id=kluslayout onchange="emphasizeSend()">
        <tr>
        <td rowspan=3 width=90>
          <div class="iconmenu menu">
            <img src="/img/{escape(klus.icon)}.png" id=bigicon>
            <input type=hidden name=icon id=icon value="{escape(klus.icon)}">
           <div id=iconlist onclick="closeMenu(this);emphasizeSend()">
        ''')
    yield from iconEntries(klusDB, klus.icon)
    # context, goal
    yield dedent('''\
          </div>
         </div>
        </td><td width=58>
         Context:
        </td><td width=98>
         <select name=context id=context required title=Context>
        ''')
    yield from makeOption(klusDB.contexten(), klus.context)
    yield dedent('''\
        </td></tr><tr><td>
         Doel:
        </td><td>
         <select name=doel id=doel required title=Doel>
        ''')
    yield from makeOption(klusDB.doelen(), klus.doel)
    # energy, done, postpone, previous
    yield dedent(f'''\
        </td></tr><tr><td>
         Energie:
        </td><td>
         <input type=range id=energie
         name=energie value="{klus.energie}"
         min=0 max=255>
        </td></tr><tr><td>
         Gedaan op:
         <input type=datetime-local class=dt
         name=gedaan id=gedaan value="{klus.html('gedaan')}">
        </td><td>
         Doen ná:
        </td><td>
         <input list=tasklist name=eerst class=zoekklus placeholder="(klus)"
         value="{escape(klusDB[klus.eerst].naam) if klus.eerst in klusDB else ""}">
        </td></tr>
        <datalist id=tasklist>
        <option value="(Geen)">
        ''')
    for i,k in klusDB.items():
        if i == ID: continue
        yield f'<option value="{escape(k.naam)}">\n'
    # repeat, startdate, min/max, previous
    yield dedent(f'''\
        </datalist>
        <tr class=yellow><td>
          <label>
          <input type=checkbox id=herhaal name=herhaal
          onchange="showRepOrNot()" {klus.html('herhaal')}>
          Herhaal</label>
        </td><td class=noRepText>
          <label for=vanaf>Vanaf:</label>
        </td><td class=noRepText>
          <input type=datetime-local class=dt
           name=vanaf id=vanaf value="{klus.html('vanaf', 0)}"
           onchange="vantotChanged()">
        </td><td class=repText>
          <label for=min># dagen:</label>
        </td><td class=repText>
          <input type=number class=num
           name=min id=min value="{klus.html('min')}"
           min=1 onchange="repChanged(true)">
           -
          <input type=number class=num
           name=max id=max value="{klus.html('max')}"
           min=2 onchange="repChanged(true)">
        </td></tr>
        <tr class=yellow><td>
        ''')
    # quickmenu
    yield quickMenu()
    # enddate, planned, notes
    yield dedent(f'''\
        </td><td class=noRepText>
          <label for=tot>Uiterlijk:</label>
        </td><td class=noRepText>
          <input type=datetime-local class=dt
          name=tot id=tot value="{klus.html('tot', 10080)}"
          onchange="vantotChanged()">
        </td><td class=repText>
          <label for=gepland>Ingepland:</label>
        </td><td class=repText>
          <input type=datetime-local class=dt name=gepland id=gepland value="{klus.html('gepland')}">
        </td></tr>
        </table>
        <div id=notediv>
        <textarea name=notitie id=notitie onkeyup="emphasizeSend()"
         rows=7 placeholder="Aantekeningen"
         >{klus.html('notitie')}</textarea>
        </div>
        ''')

    # lower button row
    yield ' <div id=taskbuttons>'
    # potential waiting tasks
    later = [(i,k)
             for i,k in klusDB.items()
             if k.eerst == ID]
    yield dedent(f'''\
        <span class="menu right">
        <button type=button{(not later)*" disabled"}>
        Hierna</button>
        ''')
    if later:
        yield ('<div><ul>' +
               '\n'.join(f'<li><a href="/edit/{i}">{k.naam}</a></li>'
               for i,k in later)
               + '</ul></div>')
    # earlier repetitions
    yield dedent(f'''\
        </span>
        <span class="menu medium">
        <button type=button{(not klus.eerderGedaan)*" disabled"}>
        Eerder</button>
        ''')
    if klus.eerderGedaan:
        yield ('<div><ol>' +
               '\n'.join(f'<li>{d}</li>'
               for d in reversed(klus.eerderGedaan))
               + '</ol></div>')
    # show checklist
    yield dedent(f'''\
        </span>
        <span class="menu middle">
        <button type=button{(not klus.stappen)*" disabled"}>
        Stappen</button>
        ''')
    if klus.stappen:
        yield '<div><ol id=subtasks>'
        yield klus.fields['stappen'].toHTML(klus.stappen)
        yield '</ol></div>'
    # edit/add checklist
    yield dedent(f'''\
        </span>
        <a href="/steps/{ID}">
            {'Wijzig' if klus.stappen else 'Voeg toe'}
        </a>
        </div>
        ''')
    # remarks
    # compute deadline
    if klus.herhaal:
        if klus.gedaan:
            deadline = klus.gedaan + timedelta(days=klus.max)
        else:
            deadline = None
    else:
        deadline = klus.tot
    deadline = deadline.strftime('%Y-%m-%d@%H:%M') if deadline else "--"
    yield dedent(f'''\
        <p class=remark id=extrainfo>
          Relevantie: {klus.relevantie():.1%} * {exp(klusDB.priorities[klus.doel] / 6):.1f};
          deadline: {deadline}.
        <br>
          Iconnaam: <code>{klus.icon}</code>
        </p>
        <script>showRepOrNot()</script>
        ''')

script = dedent('''\
    // auxiliary function: add offset in hours to JavaScript date
    // and convert to html format
    function dateOffset2html(date, offset) {
      // undo the time zone conversion of ISOString
      let revTimeZone = new Date(date.getTime()
        + offset * 3.6e6
        - date.getTimezoneOffset() * 6e4);
      // cut off seconds and Z
      return revTimeZone.toISOString().slice(0, -8);
    }

    function closeMenu(m) {
      m.classList.remove("show");
    }

    function iconClicked(name) {
      document.getElementById("icon").value = name;
      document.getElementById("bigicon").src = "/img/" + name + ".png";
      document.getElementById("iconlist").styleList.remove("show");
    }

    function showRepOrNot() {
      let checkBox = document.getElementById("herhaal");
      let table = document.getElementById("kluslayout");
      let noRepElts = table.getElementsByClassName("noRepText");
      let repElts = table.getElementsByClassName("repText");
      var i, txt;
      txt = checkBox.checked ? "none" : "";
      for (i=0; i<noRepElts.length; i++) {
        noRepElts[i].style.display = txt;
      }
      txt = checkBox.checked ? "" : "none";
      for (i=0; i<repElts.length; i++) {
        repElts[i].style.display = txt;
    } }
    ''') + dedent(f'''\
    function vantotChanged(clear) {{
      let vanaf = document.getElementById("vanaf");
      vanaf.max = dateIDoffset("tot", -{MINHOURS});
      let tot = document.getElementById("tot");
      tot.min = dateIDoffset("vanaf", {MINHOURS});
      vanaf.setCustomValidity(
        vanaf.max && tot.min && !tot.checkValidity()
        ? "Eindtijd moet minstens zes uur na het begin zijn"
        : "");
      if (clear) document.getElementById("extrainfo").innerHTML = "";
    }}
    ''') + dedent('''\
    function repChanged(clear) {
      let minElem = document.getElementById("min");
      let maxElem = document.getElementById("max");
      minElem.setCustomValidity(
        Number(maxElem.value||2) - Number(minElem.value||1) < 1
        ? "Er moet minstens één dag tussen minimum en maximum zitten"
        : "");
      if (clear) document.getElementById("extrainfo").innerHTML = "";
    }
    // fetch date from (datetime-local) input
    // apply offset given in hours,
    // and convert back to datetime-local string
    function dateIDoffset(id, offset) {
      let value = document.getElementById(id).value;
      if (!value) return "";
      return dateOffset2html(new Date(value), offset);
    }
    // set van/tot for quickMenu
    function setTaskPeriod(low, high) {
      let herhaal = document.getElementById("herhaal");
      let date = new Date();
      if (herhaal.checked) {
        // count around noon
        date.setHours(12);
        let gepland = document.getElementById("gepland");
        gepland.value = dateOffset2html(date, 24 * low);
        if (low < high) alert("Gepland op eerste dag");
      } else {
        // count from 6am
        date.setHours(6);
        let vanaf = document.getElementById("vanaf");
        vanaf.value = dateOffset2html(date, 24 * low);
        // count until 7pm
        date.setHours(19);
        let tot = document.getElementById("tot");
        tot.value = dateOffset2html(date, 24 * high);
      }
      document.getElementById("extrainfo").innerHTML = "";
    }
    function setPeriod() {
      let minElem = document.getElementById("min");
      let maxElem = document.getElementById("max");
      let gedaan = document.getElementById("gedaan");
      let herhaal = document.getElementById("herhaal");
      let period = ((new Date()).getTime() - (new Date(gedaan.value)).getTime()) / 8.64e7;
      herhaal.checked = true;
      showRepOrNot();
      minElem.value = Math.floor(period * .8);
      maxElem.value = Math.ceil(period * 1.2);
    }

    function setContextGoal(context, goal) {
      document.getElementById("context").value = context;
      document.getElementById("doel").value = goal;
    }
    ''')


def post(forms, klusDB, klusID):
    """Process post data
    """
    klus = klusDB[forms.ID]
    # fill in all fields, but
    # gedaan and eerst need some fixing afterwards
    oldgedaan = klus.gedaan
    klus.fromPost(forms)
    if klus.eerderGedaan and not forms.gedaan:
        logging.info("Deleting date from %s: %s", klus.naam, oldgedaan)
        # journal erasure
        klusDB.journal(forms.ID)
    newGedaan, klus.gedaan = klus.gedaan, oldgedaan
    klus.addGedaan(newGedaan)
    # convert eerst to klusID or None
    if forms.eerst:
        if forms.eerst == '(Geen)':
            klus.eerst = None
        else:
            klus.eerst = next(
                ID
                for ID,klus in klusDB.items()
                if klus.naam == forms.eerst)
    #make sure klus[ID] is updated and saved before return
    try:
        if forms.menu == "delete":
            logging.info("deleting %s", klus.naam)
            klusDB.deleted = forms.ID
            klus.verwijderd = True
            message = quote_plus(
                f'Klus {openQuote}{klus.naam}{closeQuote} verwijderd. '
                'Gebruik het menu om dit ongedaan te maken.')
            return (forms.returnto or "/nu") + '?m=' + message
        elif forms.menu == "copy":
            logging.info("copying %s", klus.naam)
            newID = klusDB.newID()
            klusDB[newID] = klus.copy()
            klusDB.journal(newID)
            return f'/edit/{newID}'
        else:
            return forms.returnto
    finally:
        klusDB.journal(forms.ID)
        klusDB.move_to_end(forms.ID)
        klusDB.flush()
