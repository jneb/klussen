#qpy:webapp: Klussenlijst
#qpy://localhost:8080
"""
This program implements Jurjen's version
of Life Balance
See design.txt for explanations
"""

import os
from pathlib import Path
fileRoot = Path(__file__).parent
os.chdir(fileRoot)

import logging
from general import HTTP, dedent, generatorGzipper

from bottle import (default_app,
    HTTPResponse, response, redirect,
    run, request)

import re
import email.utils
from datetime import datetime
from html import escape

# modules
import nu, alles, klaar, edit, steps, doel, context, icons, about
# layout
import navbar, bottom, klusdb
from icons import downloadIcon

# link between pages and modules
pageModule = {
    module.__name__: module
    for module in (nu, alles, klaar, edit, steps, doel, context, icons, about)
    }

allModules = (*pageModule.values(), navbar, bottom)

######### Profiler ##########
def profileGen(f):
    logging.info("Profiling function %s", f.__name__)
    import cProfile
    def func(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        try:
            return list(f(*args, **kwargs))
        finally:
            pr.disable()
            pr.dump_stats("stats")
    return func

######### routers for "fixed" files  ###############
APP = default_app()

def logreq(level=logging.INFO):
    ims = request.headers.get('If-Modified-Since')
    try: parsed = email.utils.parsedate_to_datetime(ims)
    except TypeError: parsed = "?"
    logging.log(level,
        "%s %s?%s:%s",
        request.method,
        request.path,
        request.query_string,
        ims and parsed)

def reportServerException(serveFunc):
    """Log requests and report exceptions
    """
    def resultFunc(*args, **kwargs):
        logreq(HTTP)
        try:
            return serveFunc(*args, **kwargs)
        except HTTPResponse:
            raise
        except Exception:
            logging.error(
                "Error serving %s %s?%s",
                request.method,
                request.path,
                request.query_string,
                exc_info=True)
            raise
    return resultFunc
        
@APP.get('/__exit')
def __exit():
    """required by qpython
    """
    logreq()
    yield("<h2>Server stopped. Nothing to see here.")
    SERVER.server_close()
    about.clearDiffs()

@APP.get('/favicon.ico')
def serveFavicon():
    """to prevent page not found
    """
    logreq()
    return

@APP.get('/img/<filename>')
@reportServerException
def serveImage(filename):
    """Find the icon
    Sends the file directly from the zip,
    caching is set to a week
    sends actual file if not in zip
    """
    with KLUSDB.ziprLock:
        try:
            fileinfo = KLUSDB.iconzip.getinfo(filename)
            size = fileinfo.file_size
            dt = datetime(*fileinfo.date_time)
            fetch = lambda: KLUSDB.iconzip.read(fileinfo)
        except KeyError:
            path = Path('img') / filename
            if not path.is_file():
                logging.warning("Missing: %s", path)
                response.status = 404
                return
            logging.info('fetching actual file')
            size = path.stat().st_size
            dt = datetime.fromtimestamp(path.stat().st_mtime)
            fetch = lambda: open(path,'rb').read()
        if handleCache(dt, 86400, -1):
            return
        response.set_header('Content-Length', size)
        response.set_header('Content-Type',
          'image/svg+xml; charset=utf-8' if filename[-3:] == 'svg' else 'image/png')
        response.set_header('X-Content-Type-Options', 'nosniff')
        yield fetch()
    logging.log(HTTP, "sent file %s", filename)

@APP.get('/inc/style.css')
@reportServerException
def serveStyle():
    """Generate style file
    """
    dt = datetime.fromtimestamp(
        max(os.path.getmtime(__file__),
            max(os.path.getmtime(m.__file__)
                for m in allModules
                if m.style)))
    if handleCache(dt, 60, 3600):
        return
    response.set_header('Content-Type', 'text/css; charset=utf-8')
    response.set_header('X-Content-Type-Options', 'nosniff')
    yield styleBlurb
    for module in allModules:
        yield module.style
    logging.info("style sent")

@APP.get('/inc/script.js')
@reportServerException
def serveScript():
    """Generate script file
    """
    # computer last modified date
    dt = datetime.fromtimestamp(
        max(os.path.getmtime(m.__file__)
            for m in allModules
            if m.script))
    if handleCache(dt, 60, 3600):
        return
    response.set_header('Content-Type', 'text/javascript; charset=utf-8')
    response.set_header('X-Content-Type-Options', 'nosniff')
    for module in allModules:
        yield module.script
    logging.info("script sent")

@APP.get('/inc/<file>')
@reportServerException
def serveFile(file):
    """Serve actual files in inc directory
    """
    logreq()
    path = Path('inc') / file
    mtime = path.stat().st_mtime
    dt = datetime.fromtimestamp(mtime)
    if handleCache(dt, 900, 86400):
        return
    t = {".js": "javascript",
         ".html": "html"}[path.suffix]
    response.set_header('Content-Type', f'text/{t}; charset=utf-8')
    response.set_header('X-Content-Type-Options', 'nosniff')
    # use gzip (=deflate) if possible
    allowedEncodings = request.headers.get('Accept-Encoding')
    if 'gzip' in allowedEncodings or 'deflate' in allowedEncodings:
        logging.info("Sending compressed library %s", file)
        encoding = 'gzip' if  'gzip' in allowedEncodings else 'deflate'
        response.set_header('Content-Encoding', encoding)
        return generatorGzipper([path.read_bytes()])
    else:
        return path.read_text()
    

@APP.get('/fetch')
@reportServerException
def serveIcon():
    """Get icon, return next page
    """
    logreq()
    query = request.query
    next, klusID, message = downloadIcon(KLUSDB, query.name, query.desc, query.id)
    if klusID: return sendpage(pageModule[next], klusID=klusID)
    elif message: return sendpage(nu, message=message)

IDpat = re.compile('[A-Z0-9]{2}')
######### routers for pages  ###############
@APP.get('/')
@reportServerException
def serveHome():
    # make sure page is refreshed at start of program
    logreq()
    KLUSDB.flush()
    redirect("/nu")

@APP.get('/<page>/<klusID>')
@reportServerException
def serveKlusPage(page, klusID):
    """edit and steps
    """
    logreq()
    if not IDpat.fullmatch(klusID):
        return HTTPResponse(405)
    # enable delete task for edit
    KLUSDB.deleted = None
    return sendpage(pageModule[page], klusID=klusID)


@APP.get('/<page>')
@reportServerException
def servePage(page="nu"):
    logreq()
    return sendpage(pageModule[page])

@APP.post('/<page>')
@APP.post('/<page>/<klusID>')
@APP.post('/<page>/<klusID>/<term>')
@reportServerException
def postPage(page, klusID=None, term=None):
    logreq()
    if term: klusID = None
    if klusID and not IDpat.fullmatch(klusID):
        return HTTPResponse(405)
    # process data. post may output href of next page
    forms = request.forms.decode()
    logging.debug("klusID: %s, Post data: %s", klusID, '\n'.join(a+'='+b for a,b in forms.items()))
    # process post actions from page
    href = pageModule[page].post(forms, KLUSDB, klusID)
    logging.debug("page.post returns %s", href)
    # save klus (for edit page)
    ID = forms.ID
    if ID in KLUSDB:
        KLUSDB.journal(ID)
        logging.info("Saved %s: %s", ID, KLUSDB[forms.ID].dump())
    elif ID:
        logging.warning("Couldn't save: %s", ID)
    # new klus
    newID = bottom.post(forms, KLUSDB)
    if newID:
        KLUSDB.journal(newID)
        logging.info("New task %s = %s", newID, forms.new)
        href = '/edit/' + newID
    redirect(href or "/nu")


@APP.get("/icons/<style>/<term>")
@reportServerException
def searchIcons(style, term):
    logreq()
    return sendpage(icons, klusID=(style, term))

# to allow to switch to alles page with selection
@APP.get("/alles/context")
@reportServerException
def fetchContext():
    logreq()
    contextCode = request.query.q
    KLUSDB.suppressed = ''.join(
        chr(c)
        for c,ctxt in enumerate(KLUSDB.contexten(), start=97)
        if chr(c) != contextCode) + ';'
    KLUSDB.flush()
    redirect("/alles")

@APP.get("/alles/doel")
@reportServerException
def fetchDoel():
    logreq()
    doelCode = request.query.q
    KLUSDB.suppressed = ';' + ''.join(
        chr(c)
        for c,ctxt in enumerate(KLUSDB.doelen(), start=97)
        if chr(c) != doelCode)
    KLUSDB.flush()
    redirect("/alles")

@APP.post("/undo")
@reportServerException
def undo():
    ID = KLUSDB.deleted
    KLUSDB[ID].verwijderd = False
    KLUSDB.journal(ID)
    KLUSDB.deleted = None
    redirect('/edit/' + ID)

######### boilerplate  ###############
styleBlurb = dedent('''\
    :root {
      --bodymargin: 3px;
      --menuheight: 45px;
      --bottomheight: 0px;
      --titlefs: 20pt;
      --menufs: 17pt;
      --entryfs: 15pt;
      --normal: 13pt;
      --small: 12pt }
    * { box-sizing: border-box;
        border: 0px dotted red }
    body {
      font-family: sans-serif;
      font-size: var(--normal);
      margin: var(--bodymargin);
      margin-top: calc(10px + var(--menuheight));
      margin-bottom: var(--bottomheight) }
    img { object-fit: contain }
    h2 { font-size: var(--menufs) }
    button { font-size: inherit }
    input[type=text], select { font-size: var(--entryfs) }
    input { border: 1px solid grey }
    .message { color: blue }
    ''')
    
HTMLblurb = dedent('''\
    <!DOCTYPE html>
    <html lang=nl>
    <head>
    <meta charset="UTF-8">
    <title>Jurjen's Planning program</title>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name=author content="Jurjen N.E. Bos">
    <link rel="stylesheet" type=text/css href="/inc/style.css">
    <link rel="icon" href="/img/konijn.svg" type=image/png>
    </head>
    <body>
    <script src="/inc/script.js"></script>
    ''')

################# generate pages ############

def handleCache(pageDate, maxage, staleAge):
    """handle caching headers
    returns True if no generation is needed
    if handleCache(mtime, maxage, stale):
        return
    yield resource
    where:
        mtime: datetime last modified
               will be rounded down to seconds
               assumed to be local time
        maxage: put in Cache-Control:max-age
        staleAge: put in Cache-Control:stale-while-revalidate
                  if -1, set immutable
    """
    if CACHE:
        lmtime = email.utils.localtime(pageDate.replace(microsecond=0))
        response.set_header('Last-Modified',
            email.utils.format_datetime(lmtime))
        # allow view before reload for a minute 
        if staleAge < 0: part2 = 'immutable'
        else: part2 = f'stale-while-revalidate={staleAge}'
        response.set_header('Cache-Control',
            f'max-age={maxage}; ' + part2)
        ims = request.headers.get('If-Modified-Since')
        if ims:
            if ';' in ims:
                logging.warning("fixing weird time format: %s", ims)
                ims = ims.split(';')[0]
            ims = email.utils.parsedate_to_datetime(ims)
            if ims >= lmtime:
                response.set_header('Date',
                    email.utils.localtime())
                response.status = 304
                logging.log(HTTP, "Sending 304 for %s %s/%s",
                   	request.method,
                    request.path,
                    request.query_string)
                return True
    if request.method == 'HEAD':
        logging.log(HTTP, "Sending only headers for %s %s/%s",
            request.method,
            request.path,
            request.query_string)
        return True
    # page needs to be generated
    logging.log(HTTP, "Constructing data for %s %s/%s",
        request.method,
        request.path,
        request.query_string)
    return False


def sendpage(page:"module", *, klusID=None, message=None):
    """Produce the actual server page
    with optional extra message
    """
    if message is None:
        message = request.query.m
    logging.log(HTTP,
        'Sending /%s/%s?m=%s',
        page.__name__,
        klusID,
        message[:10])
    if handleCache(KLUSDB.timestamp, 0, 60):
        return
    # now generate the page
    yield HTMLblurb
    yield from navbar.body(
        KLUSDB,
        page.__name__.capitalize(),
        LOGLEVEL < logging.WARNING)
    if message:
        yield '<p class=message>' + escape(message) + '</p>'
    # make sure the action matches the page
    # prevent click hijack
    yield dedent(f'''\
        <form action="" method="post" id=theform>
        <input type=hidden id=suppressed name=suppressed value="{KLUSDB.suppressed}">
        ''')
    if klusID:
        returnto = request.query.returnto if 'returnto' in request.query else None
        # special treatment for edit and icons
        yield from page.body(KLUSDB, klusID, returnto=returnto)
    else:
        yield from page.body(KLUSDB)
    yield from bottom.body(page.__name__.capitalize())
    yield "</form></body></html>"
    logging.info("Sent data for %s", page.__name__)


########## set up everything #######

KLUSDB = klusdb.KlusDB(fileRoot / "klussen.db")

def main(host="localhost"):
    """multithreading server
    """
    global SERVER
    from paste import httpserver
    logging.addLevelName(HTTP, "HTTP")
    logging.getLogger().setLevel(LOGLEVEL)
    logging.info("Address: %s:8080", host)
    logging.info(
        "level: %s, caching: %s",
        logging.getLevelName(LOGLEVEL),
        CACHE)
    SERVER = httpserver.serve(APP,
        host=host, port=8080,
        start_loop=False,
        threadpool_workers=4)
    SERVER.serve_forever()

CACHE = True

if __name__ == "__main__":
    if fileRoot.name.endswith('Prod'):
        LOGLEVEL = logging.WARNING
    else:
        LOGLEVEL = logging.DEBUG
        #LOGLEVEL = HTTP
        #LOGLEVEL = logging.INFO
        CACHE = True
    main()
