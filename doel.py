#!python3
"""Jurjen's planning program
Life goal editor
this file has some modifications for testing in qpython
"""

# intro and auxiliary routines
import klusdb
from general import makeOption, dataURI, dedent
import context
from collections import Counter
from operator import attrgetter
from html import escape
import logging

# standard routines and data

svgline = dedent("""\
    <svg xmlns="http://www.w3.org/2000/svg"
    width="2" height="25">
    <line x1="1" x2="1" y2="25" stroke="blue" />
    </svg>
    """)

style = dedent(f'''\
    td.line {{
      border-left: 1px solid blue;
      position: relative;
      left: -87px }}
    #mergetable input[type=range] {{
      width: 150px }}
    #mergetable.doel td:nth-child(3) {{
      background: url("{dataURI(svgline)}") no-repeat center
    }}
    ''')

def body(klusDB):
    yield dedent('''\
        <div id=edittable style="background-color:#fff2f1">
        <h2>Doelen:</h2>
        <div class="menu right">
        <table id=mergetable class=doel><thead>
        <tr><th>Naam
        </th><th>#
        </th><th>Prioriteit
        </th></tr>
        ''')
    counts = Counter(map(attrgetter("doel"), klusDB.values()))
    for c, doel in enumerate(klusDB.doelen(), start=97):
        yield dedent(f'''\
            <tr onclick="setupMenu('{doel}')">
            <td>{escape(doel)}
            </td><td class=numcol>
              <a href="/alles/doel?q={chr(c)}">
              {counts[doel]}\N{north east arrow}
              </a>
            </td><td><input name="doel{chr(c)}" type=range min=-4 max=4
             value="{klusDB.priorities[doel]}" onchange="emphasizeSend()">
            </td></tr>
            ''')
    yield dedent('''
        </table>
        <label><input type=text name=newdoel
          placeholder="Nieuw doel" onkeyup="emphasizeSend()"
        ></label>
        <div id=editmenu>
        ''')
    yield from makeSettingsMenu(klusDB)
    yield '</div></div></div>'


def makeSettingsMenu(klusDB):
    yield context.renameOrMergeStr
    yield from makeOption(klusDB.doelen())

# all scripts are in context.py
script = ''


def post(forms, klusDB, klusID):
    """Process post data
    """
    for c,doel in enumerate(klusDB.doelen(), start=97):
        code = 'doel' + chr(c)
        if code in forms and forms[code]:
            klusDB.priorities[doel] = int(forms[code])
    klusDB.writeHeader()
    return context.post(forms, klusDB, klusID, attr="doel")
