#!python3
"""Utility functions for klussen programma
"""

import os, re, sys
os.chdir(os.path.dirname(__file__) or '.')
from binascii import crc32

def replaceAll(old):
    """Replace part with something
    """
    old = re.compile(old, re.MULTILINE)
    total = 0
    for fn,gn in genFiles():
        f = open(fn)
        seen = False
        for lineno,line in enumerate(f):
            if old.search(line):
                if not seen: print("---File:", fn)
                print(lineno+1, end=": " + line)
                total += 1
                seen = True
        f.close()
    if not total:
        return "Not found"
    new = input("with (empty=cancel):")
    if not new:
        return "Cancelled"
    for fn,gn in genFiles():
        with open(fn) as f:
            c = f.read()
        if old.search(c):
            bak = fn + ".bak"
            if os.path.exists(bak):
                os.unlink(bak)
            os.rename(fn, bak)
            f = open(fn, 'w')
            f.write(old.sub(new, c))
            f.close()
    return f"Made {total} replacements"

def genFiles(onlyDifferent=False):
    for name in os.listdir('.'):
        if not name.endswith('.py'): continue
        if name.startswith('.'): continue
        prod = os.path.join('..', 'klussenProd', name)
        if not os.path.exists(prod): continue
        if onlyDifferent and filecrc(name) == filecrc(prod):
            continue
        yield name, prod

def search(pat):
    """Find pattern in source with nice display
    """
    for name, prod in genFiles():
        with open(name) as sourceFile:
            # clip off .py
            name = name[:-3] + ':   '
            for lineno,line in enumerate(sourceFile, start=1):
                m = re.search(pat, line)
                if not m: continue
                line = line.rstrip()
                start = m.start()
                # clip off necessary digit space
                name = name[:9-len(str(lineno))]
                print(name + str(lineno),
                     '^' * (8-start) +
                     line[max(start-8, 0):start+26])

def collect(t, outfile=sys.stdout):
    """Xref HTML IDs or names in use
    """
    from collections import defaultdict
    from itertools import groupby
    from operator import itemgetter
    import klusdb
    print("xref of", t, file=outfile)
    pat = re.compile({
        'id': r'^#(\w+)\b[^:]'
             r'|[^\'"&]#(\w+)\b'
             r'|id="(\w+)\b'
             r'|for="(\w+)\b'
             r'|getElementById\(["\'](\w+)\b'
             r'|toggleShow\(["\'](\w+)\b',
        'class': r'^[a-z, ]*\.([-\w]+)\b {'
                r'|getElementByClassName\(["\'](\w+)\b'
                r'|class="([-\w]+)[ \'"]',
        'name': r'name="([-\w]+)',
        'var': r'--(\w+)',
        }[t])
    xref = defaultdict(list)
    for name, prod in genFiles():
        for lineno,line in enumerate(open(name), start=1):
            for m in pat.finditer(line):
                symbol = next(filter(None, m.groups()))
                xref[symbol].append((name[:-3], lineno))
    symbols = list(xref)
    symbols.sort()
    symbols.sort(key=klusdb.Klus.fields.__contains__)
    for symbol in symbols:
        print('-' * 20,
              symbol,
              "(field)" * (symbol in klusdb.Klus.fields),
              file=outfile)
        for name, namerefs in groupby(xref[symbol], key=itemgetter(0)):
            print(name + ':',
                  ','.join(str(lineno)
                           for name,lineno in namerefs),
                  file=outfile)

def usedIcons():
    """Find used icons in code
    The last time I checked it eas the list in the code
    """
    print("In code")
    from general import ICONZIP, fPROGAM
    namelist = sorted(zipfile.ZipFile(ICONZIP).namelist())
    
    available = set(name[:-4]
        for name in namelist)
    unused = available.copy()
    pattern = '|'.join(
        '\\b' + name
        for name in unused)
    search(r'[/\'"](' + pattern + r')(\.png)?[\'"]')
    # in the program
    unused -= {*PROGRAMICONS}
    with open('klussen.db') as f:
        for line in f:
            if line.startswith('  naam:'):
                name = line[8:-1]
            if line.startswith('  icon:'):
                if line[8:-1] not in available:
                    print("Missing icon:", line[8:-1], name)
                unused.discard(line[8:-1])
    print("Unused:", unused)

def compareAll():
    """Changes with production
    """
    import difflib
    for name, prod in genFiles(True):
    #for name,prod in [('../klussenProd/navbar.py','../klussenProd/navbar.py.bak')]:
        printedFlag = False
        for line in difflib.unified_diff(
                open(prod).readlines(),
                open(name).readlines(),
                prod,
                name,
                n=3):
            if not line.endswith('\n'):
                line += '<EOF>\n'
            print(end=line)
            printedFlag = True
        if printedFlag:
            print("\n" + "=" * 40)

def filecrc(name):
    result = 0
    with open(name, 'rb') as f:
        d = f.read(4096)
        while d:
            result = crc32(d, result)
            d = f.read(4096)
    return result

def install():
    import shutil
    for name, prod in genFiles(True):
        bak = prod + ".bak"
        if os.path.exists(bak):
            os.unlink(bak)
        os.rename(prod, bak)
        shutil.copy(name, prod)
        print(name, '->', prod)

def checkHTML(page):
    """Check HTML structure of page
    """
    from html.parser import HTMLParser
    import main
    class MyHTMLParser(HTMLParser):
        unpaired = 'link meta input img option br'.split()
        def __init__(self):
            super().__init__()
            self.stack = []
        def handle_starttag(self, tag, attrs):
            self.stack.append(tag)
        def handle_endtag(self, tag):
            while self.stack:
                needed = self.stack.pop()
                if tag == needed:
                    return
                if needed not in self.unpaired:
                    print("Expected", '/'+needed, "with tag", tag)
            raise ValueError(f"No start for {tag}")
        def handle_data(self, data):
            data = data.replace('\n','').strip()
            if len(data) > 10:
                print(data)
    parser = MyHTMLParser()
    if page == "edit":
        args = {'klusID':next(iter(main.KLUSDB))}
    else: args = {}
    #outfile = open("t","w")
    main.LOGLEVEL = 50
    for chunk in main.sendpage(main.pageModule[page], **args):
        #print(chunk, file=outfile)
        try: parser.feed(chunk)
        except ValueError as e:
            print(e)
            print(chunk)
            return
    if parser.stack:
        print("Leftover tags:", parser.stack)
    #outfile.close()

def checkStyle():
    """
    Check style file
    """
    from main import serveStyle
    identifier = r'[a-z][-a-z0-9]*'
    clause = '[.#]?' + identifier
    attribute = r'\[[a-z]+="[a-z]+"\]'
    clause = ('(' +
        clause + '|' +
        clause + ':[a-z]+|' +
        clause + ':' + identifier + r'(\([a-z]+\))?|' +
        clause + attribute +
        ')')
    selector = '(' + clause + r'( |, |\.|,\n| > | \+ ))*' + clause + ' '
    # value = '(( [-a-zA-Z]+| \d+[a-z]+| 0| rgba\([0-9 .,]+\)){1,4}| \d+%| url\([\'"a-z/.]+\))'
    value = ' [-a-zA-Z0-9 %.(),/"]+'
    line = identifier + ':' + value + ';'
    line += r'( +/\*.*?\*/)?'
    descriptor = r'{\n(  ' + line + '\n)*}'
    descriptor = ('(' +
        descriptor +
        '|{' + line + r'})\n')
    pat = r'\n*' + selector + descriptor
    count = 0
    for style in serveStyle():
        while True:
            m = re.match(pat, style)
            if not m: break
            count += 1
            style = style[m.end():]
        if style:
            print("Parsed", count, "descriptors")
            print("First style problem before:")
            print('"' + style[:30].replace('\n', '$') + '"')
            m = re.match(r'\n*' + selector + r'\{\n(  ' + line + '\n)*', style)
            if m:
                print('"' + style[m.end():m.end()+30].replace('\n', '$') + '"')
                return
    print("Parsed", count, "descriptors")

def checkEcma():
    from main import serveScript
    stack = []
    count = 0
    for chunk in serveScript():
        for i,c in enumerate(chunk):
            if c in '({[':
                stack.append(c)
            elif c in ')}]':
                match = {')':'(', '}':'{', ']':'['}[c]
                if stack.pop() == match:
                    count += 1
                    continue
                print("Trouble:", chunk[max(0,i-30):i+20])
                return
    print("OK, checked", count, "pairs")

def server(i):
    import main, klusdb, general, logging
    main.LOGLEVEL = main.HTTP
    host = "localhost"
    if i == "s!":
        from general import getLocalIP
        host = getLocalIP()
    main.main(host)
    
if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__) or '.')
    def printHelp():
        print("<enter>: Check changes with prod")
        print("/<pat>: search for pattern")
        print("f<text>: find&replace")
        print("i/c/n/v: find HTML IDs/classes/names/vars")
        print("u: used icons")
        print("h<page>: check HTML output")
        print("hs: check style")
        print("he: check ecmascript")
        print("p: dump stats")
        print("s: run server, cached (!: local network)")
        print("#: get version number klussenstappen")
        print("install: update production version")
        print("b: backup production database")
    while True:
        i = input("Choice: ")
        # delayed imports
        import zipfile

        if i == "":
            compareAll()
        elif i.startswith("/"):
            search(i[1:])
        elif i.startswith("f"):
            print(replaceAll(i[1:]))
        elif i in "icnv":
            collect({
                'i':'id',
                "c":'class',
                "n":'name',
                'v': 'var'
                }[i])
        elif i == "u":
            usedIcons()
        elif i == "install":
            install()
        elif i == "hs":
            checkStyle()
        elif i == "he":
            checkEcma()
        elif i.startswith("h"):
            checkHTML(i[1:])
        elif i == "p":
            import pstats
            stats = pstats.Stats("stats")
            stats.strip_dirs().sort_stats("cumulative").print_stats()
            # stats.strip_dirs().print_callers('getattr')
        elif i == '#':
            from androidhelper import Android
            droid = Android()
            clipboard = droid.getClipboard().result
            print("Version:", clipboard.count('[V]'))
        elif i.startswith("s"):
            server(i)
        elif i == 'b':
            import shutil
            os.replace("klussen.db", "klussen.bak")
            shutil.copy("../klussenProd/klussen.db", "klussen.db")
            os.replace("img/icons.zip", "img/icons.bak")
            shutil.copy("../klussenProd/img/icons.zip", "img/icons.zip")
        else: printHelp()
