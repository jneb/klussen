#!python3
"""Jurjen's planning program
context editor
this file has some modifications for testing in qpython
"""

import os
from general import makeOption

# intro and auxiliary routines
import requests
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = "TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:ECDHE:!COMPLEMENTOFDEFAULT"
from general import (dedent,
    ICONZIP, PROGRAMICONS, MAKEICONTASK, FETCHAMOUNT)
import klusdb
from klus import Klus
import zipfile
from html import escape
import re
import logging
from urllib.parse import quote_plus, urlencode

TIMEOUT = 3.0
# common icon styles from icons8
# first one is default
ICONSTYLES = [
    "dusk", "office", "color",
    "fluency", "doodle",
    "android", "ios",
    "material",
    "plumpy", "dotty", "carbon-copy",
    ]
MAKEICONTASK = False

# functions trying to defeat icons8
def makeFetchTable(req):
    """generate table with icons
    older version using downloaded page
    from url = f'https://icons8.com/icon/set/{quote_plus(searchterm)}/{iconstyle}'

    """
    results = req.content.decode()
    pat = f'<img alt="(.*?) icon" srcset="https://img.icons8.com/{iconstyle}/2x/(.*?)\.png 2x"'
    for m in re.finditer(pat, results):
        commonName, name = m.groups()
        checkmark = "\N{check mark}" * (name + '.png' in names)
        query = urlencode(
            {'name': name,
             'desc': commonName,
             'iconstyle': iconstyle})
        yield dedent(f'''\
            <tr><td>
              <a href="/fetch?{query}">
                {escape(commonName)}
              </a>
            </td><td>
              <img src="https://img.icons8.com/{iconstyle}/30/{name}.png">
            </td><td>
              {checkmark}
            </td><td>
              <code>{escape(name)}</code>
            </td></tr>
            ''')

def makeFetchTable(req, names, iconstyle):
    """generate table with icons
    use anonymous api call result
    table columns
    url, picture, available flag, name
    """
    result = req.json()
    if not result.get("success"):
        yield 'Sorry, failed: {result.get("success")}"'
        return
    for icon in result['icons']:
        checkmark = "\N{check mark}" * (icon["commonName"] in names)
        query = urlencode({
            'name': icon["commonName"],
            'desc': icon["name"],
            'id': icon["id"]
            })
        baseurl = "https://img.icons8.com"
        fetch30 = urlencode({
            'id': icon["id"],
            'size': 30,
            'token': "",
            'format': "png",
            'fromSite': "true"
            })
        yield dedent(f'''\
            <tr><td>
              <a href="/fetch?{query}">
                {escape(icon["name"])}
              </a>
            </td><td>
              <img src="{baseurl}?{fetch30}">
              </td><td>
              {checkmark}
            </td><td>
              <code>{escape(icon["commonName"])}</code>
            </td></tr>
            ''')

def downloadIcon(klusDB, name, desc, id):
    """download icon, add to database
    """
    path = name + '.png'
    baseurl = "https://img.icons8.com"
    fetch100 = urlencode({
        'id': id,
        'size': 100,
        'token': "",
        'format': "png",
        'fromSite': "true"
        })
    logging.info("fetching icon at %s?%s", baseurl, fetch100)
    r = requests.get(baseurl + '?' + fetch100)
    if r.status_code != 200:
        return "icons", None, f"Ophalen icon {desc} is mislukt."
    # get copy lock to decide name
    with klusDB.zipcLock:
        nl = klusDB.iconzip.namelist()
        if path in nl:
            i = -1
            while name + str(i) + '.png' in nl: i -= 1
            path = name + str(i) + '.png'
            logging.warning("Changed icon path to %s", path)
        # get update lock to modify
        with klusDB.zipuLock:
            klusDB.iconzip.close()
            zf = zipfile.ZipFile(ICONZIP, 'a')
            # png is already compressed
            zf.writestr(path, r.content, compress_type=zipfile.ZIP_STORED)
            zf.close()
            # reinstate invariant
            klusDB.iconzip = zipfile.ZipFile(ICONZIP)
    klusDB.flush()
    message = f"Icon {desc} aangemaakt."
    if MAKEICONTASK:
        klus = klusdb.Klus("Icon: " + desc)
        klusID = klusDB.newID()
        klus.icon = name
        klusDB[klusID] = klus
        page = "edit"
    else:
        page, klusID = "nu", None
    return page, klusID, message

# internal functions
def removeFromZip(klusDB, names):
    """Remove icons with any of the given names
    from icon file.
    Icon file is open again after this call
    Touches klusDB database
    """
    with klusDB.zipcLock:
        zf = klusDB.iconzip
        newzf = zipfile.ZipFile("img/newicons.zip", "w")
        for name in set(zf.namelist()):
            item = zf.getinfo(name)
            if item.filename[:-4] in names: continue
            buffer = zf.read(item.filename)
            # png is already compressed
            newzf.writestr(item, buffer, compress_type=zipfile.ZIP_STORED)
        newzf.close()
        with klusDB.zipuLock:
            zf.close()
            os.replace(ICONZIP, 'img/icons.bak')
            os.rename("img/newicons.zip", ICONZIP)
            logging.warning("Icons removed. Old icons in icons.bak")
            # reinstate invariant
            klusDB.iconzip = zipfile.ZipFile(ICONZIP)
    klusDB.flush()


def listIcons(names, allowed=True, cls=None):
    yield '<div>'
    for name in names:
        yield f'<img src="/img/{escape(name)}.png" '
        # keep rabbit shape as it needs to be
        yield f'height=30 width=30 '
        if allowed:
            yield 'onclick="this.classList.toggle(\'disable\');buildDeleteList()" '
            yield f'name="{escape(name)}" '
        if cls:
            yield f'class="{cls}" '
        yield 'loading=lazy>'
    yield '</div>'

# standard routines and data
style = dedent('''\
    .iconsearch {
      background-image: url("/img/search.png");
      background-repeat: no-repeat;
      background-size: 40px;
      min-width: 10em;
      font-size: var(--normal);
      padding: 8px 12px 8px 42px;
      text-decoration: none;
      border: 1px solid Grey;
      border-radius: 4px }
    img.disable {
      opacity: 30%;
      background: red;
      transition: opacity, background 2s }
    ''')


def body(klusDB, styleAndTerm=None, returnto=None):
    """Body code.
    Variable.
    Uses screen scraping;
    apparently, they don't want to give me an API key
    """
    logging.debug("icons.body(%s, %s)", styleAndTerm, returnto)
    iconstyle, searchterm = styleAndTerm if styleAndTerm else (ICONSTYLES[0], "")
    with klusDB.ziprLock:
        names = klusDB.iconzip.namelist()
    yield dedent(f'''\
        <h2>Icons:</h2>
        <p>Er zijn {len(names)} icons.</p>
        <p>Icons gebruikt door het programma
        (kunnen niet worden gewist):</p>
        ''')
    yield from listIcons(PROGRAMICONS, allowed=False)
    yield '<p>Icons gebruikt door de klusjes:</p>'
    allIcons = set(n[:-4]
                   for n in names)
    klusIcons = set()
    for ID,k in klusDB.items():
        if not isinstance(k, Klus): continue
        if k.verwijderd: continue
        klusIcons.add(k.icon)
        query = urlencode({
            'message':
                "Het icon van deze klus ontbreekt. "
                "Als het toch hier verschijnt, "
                "dan is dat tijdelijk!",
            'returnto': '/icons',
            })
        if k.icon not in allIcons:
            yield dedent(f'''\
                <p>Waarschuwing: Icon van klus
                <a href="/edit/{ID}?{query}">
                "{escape(k.naam)}"
                </a>
                ontbreekt:
                <code>{escape(k.icon)}</code>
                </p>
                ''')
    yield from listIcons(klusIcons - PROGRAMICONS & allIcons)
    yield '<p>Icons niet in gebruik:</p>'
    yield from listIcons(allIcons - PROGRAMICONS - klusIcons, cls="unused")
    yield dedent(f'''\
        <p>Selecteer &eacute;&eacute;n of meer icons hierboven om te wissen.</p>
        <button type=button class=bigbutton onclick="selectUnused()">
        Wis alle ongebruikte icons
        </button>
        <input type=hidden name=delete
         value="" id=delete>
        <p>Zoek nieuw icon om toe te voegen:</p>
        <label>Zoekterm (Engels)
        <input type=text name=searchterm placeholder="Keyword"
         class=iconsearch value="{escape(searchterm)}"
         onkeyup="emphasizeSend()">
        </label>
        <p><label>Type icon:
        <select name=iconstyle required>
        ''')
    yield from makeOption(ICONSTYLES, iconstyle)
    yield '</label></p>'
    if not searchterm:
        yield dedent('''\
            <p>Ik zoek icons in
            <a href=https://icons8.com>icons8.com</a>.</p>
            ''')
        return

    # there's a search term, search and generate table
    url = f'https://search.icons8.com/api/iconsets/v5/search?term={quote_plus(searchterm)}&amount={FETCHAMOUNT}&offset=0&platform={iconstyle}&language=en-US&authors=all'
    logging.info("fetching icons at %s", url)
    try:
        req = requests.get(url, timeout=TIMEOUT)
    except requests.exceptions.Timeout:
        yield '<p>Verzoek duurde te lang. Probeer het nog eens.</p>'
        return
    if req.status_code == 404:
        yield '<p>Niet gevonden. Probeer een andere zoekterm. '
        yield '(Engels werkt het beste.)</p>'
        return
    yield dedent('''\
        <table><thead><tr>
          <th>Fetch</th>
          <th></th>
          <th>Avail&ndash;<br>able</th>
          <th>Internal name</th>
        </tr></thead><tbody>
        ''')
    yield from makeFetchTable(req, allIcons, iconstyle)
    yield '</tbody></table>'


script = dedent('''\
    function selectUnused() {
      let unused = document.getElementsByClassName("unused");
      var i;
      for (i=0; i<unused.length; i++) {
        unused[i].classList.add("disable");
      }
      buildDeleteList();
    }
    function buildDeleteList() {
      let selected = document.getElementsByClassName("disable");
      var i, result = "";
      for (i=0; i<selected.length; i++) {
        result += selected[i].name + ",";
      }
      document.getElementById("delete").value = result;
      emphasizeSend();
    }
    ''')


def post(forms, klusDB, klusID):
    """Process post data
    """
    logging.debug("forms=%s", dict(forms))
    if forms.delete:
        deleted = forms.delete[:-1].split(',')
        removeFromZip(klusDB, deleted)
    if forms.searchterm:
        return f'/icons/{escape(forms.iconstyle)}/{escape(forms.searchterm)}'
    if forms.delete:
        message = quote_plus("Icons bijgewerkt.")
        return '/icons?m=' + message 
    return '/icons'
